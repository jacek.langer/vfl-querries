-- noinspection SqlNoDataSourceInspectionForFile

-- DO NOT CHANGE THE ORDER!

USE testvfl;

-- Document_Area (create before Document_Category)
INSERT INTO document_area (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'Mod.', N'Mod.', N'Modellierung', N'Modeling',
        N'Dokumente für die Modellierung.',
        N'Documents for modeling.');
INSERT INTO document_area (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'Prom.', N'Doc. Deg.', N'Promotion', N'Doctoral Degree',
        N'Dokumente für die Promotion.',
        N'Documents for the doctoral degree.');

-- Document Category
INSERT INTO document_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_area_id)
VALUES (N'Modulbeschreibung', N'Module Description', N'Modulbeschreibung', N'Module Description',
        N'Dokumente für die Modulbeschreibungen.', N'Documents for the module descriptions.', 1);
INSERT INTO document_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_area_id)
VALUES (N'Modulhandbuch', N'Module handbook', N'Modulhandbuch', N'Module handbook',
        N'Dokumente für die Modulhandbücher.', N'Documents for the module handbooks.', 1);
-- Document Type
INSERT INTO document_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en, document_category_id, belongs_to_organization_id)
VALUES (N'TUDa Mod. Beschr.', N'TUDa Mod. Descr.', N'Offizielle TUDa Modulbeschreibung',
        N'Official TUDa module description',
        N'Offizielle Modulbeschreibung der TU Darmstadt (verabschiedet im Senat)',
        N'Official module description of the TU Darmstadt (approved by the senate)', 1, 1);
INSERT INTO document_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en, document_category_id, belongs_to_organization_id)
VALUES (N'FB18 Mod. Beschr.', N'FB18 Mod. Descr.', N'FB18-spezifische Modulbeschreibung',
        N'Dep. 18 specific Module description',
        N'Modulbeschreibungen des Fachbereichs 18',
        N'Module description for the department 18', 1, 2);
INSERT INTO document_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en, document_category_id, belongs_to_organization_id)
VALUES (N'TUDa Mod. Handb.', N'TUDa Mod. Handb.', N'Offizielles TUDa Modulhandbuch',
        N'Official TUDa module handbook',
        N'Offizielles Modulhandbuch der TU Darmstadt',
        N'Official module handbook of the TU Darmstadt', 2, 1);
INSERT INTO document_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en, document_category_id, belongs_to_organization_id)
VALUES (N'FB18 Mod. Handb.', N'FB18 Mod. Handb.', N'FB18-spezifisches Modulhandbuch',
        N'Dep. 18 specific Module handbook',
        N'Modulhandbuch des Fachbereichs 18',
        N'Module handbook for the department 18', 2, 2);

-- Document_Template
INSERT INTO document_template (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_type_id, belongs_to_organization_id)
VALUES (N'TUDa LaTeX Mod. Bes.', N'TUDa LaTeX Mod. Des.',
        N'Offizielle TUDa LaTeX Modulbeschreibung',
        N'Official TUDa LaTeX Module Description',
        N'Offizielle TUDa Modulbeschreibung basierend auf einer LaTeX-Vorlage',
        N'Official TUDa Module Description based on a LaTeX template', 1, 1);
INSERT INTO document_template (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_type_id, belongs_to_organization_id)
VALUES (N'FB18 LaTeX Mod. Bes.', N'FB18 LaTeX Mod. Des.',
        N'FB18-spezifische LaTeX Modulbeschreibung',
        N'Dep. 18 specific LaTeX Module Description',
        N'Modulbeschreibung des Fachbereichs 18 (etit) basierend auf einer LaTeX-Vorlage',
        N'Module Description for Department 18 (etit) based on a LaTeX template', 2, 2);
INSERT INTO document_template (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_type_id, belongs_to_organization_id)
VALUES (N'TUDa LaTeX Mod. Hb.', N'TUDa LaTeX Mod. Hb.', N'Offizielles TUDa LaTeX Modulhandbuch',
        N'Official TUDa LaTeX Module Handbook',
        N'Offizielles TUDa Modulhandbuch basierend auf einer LaTeX-Vorlage',
        N'Official TUDa Module Handbook based on a LaTeX template', 3, 1);
INSERT INTO document_template (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                               description_en, document_type_id, belongs_to_organization_id)
VALUES (N'FB18 LaTeX Mod. Hb.', N'TUDa LaTeX Mod. Hb.', N'FB18-spezifisches LaTeX Modulhandbuch',
        N'Dep. 18 specific LaTeX Module Handbook',
        N'Modulhanbuch des Fachbereichs 18 (etit) basierend auf einer LaTeX-Vorlage',
        N'Module Handbook for Department 18 (etit) based on a LaTeX template', 4, 2);


-- Document_template_has_setting_key
INSERT INTO document_template_has_setting_key (document_template_id, setting_key_id)
VALUES (1, 20);
INSERT INTO document_template_has_setting_key (document_template_id, setting_key_id)
VALUES (2, 20);
INSERT INTO document_template_has_setting_key (document_template_id, setting_key_id)
VALUES (3, 20);
INSERT INTO document_template_has_setting_key (document_template_id, setting_key_id)
VALUES (4, 20);


-- Part_Key
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_DESC_TEX_1', N'TUDa_MOD_DESC_TEX_1',
        N'Start der TUDa Modulbeschreibung in LaTeX',
        N'Start of the TUDa module description in LaTeX',
        N'Start der TUDa Vorlage für Modulbeschreibungen in LaTeX.',
        N'Start of the TUDa template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.begin');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_DESC_TEX_2', N'TUDa_MOD_DESC_TEX_2',
        N'Hauptteil der TUDa Modulbeschreibung in LaTeX',
        N'Main part of the TUDa module description in LaTeX',
        N'Hauptteil der TUDa Vorlage für Modulbeschreibungen in LaTeX.',
        N'Main part of the TUDa template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.main');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_DESC_TEX_3', N'TUDa_MOD_DESC_TEX_3', N'Ende der TUDa Modulbeschreibung in LaTeX',
        N'End of the TUDa module description in LaTeX',
        N'Ende der TUDa Vorlage für Modulbeschreibungen in LaTeX.',
        N'End of the TUDa template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.end');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_DESC_TEX_1', N'FB18_MOD_DESC_TEX_1',
        N'Start der FB 18 Modulbeschreibung in LaTeX',
        N'Start of the FB 18 module description in LaTeX',
        N'Start der FB 18 Vorlage für Modulbeschreibungen in LaTeX.',
        N'Start of the FB 18 template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.begin');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_DESC_TEX_2', N'FB18_MOD_DESC_TEX_2',
        N'Hauptteil der FB 18 Modulbeschreibung in LaTeX',
        N'Main part of the FB 18 module description in LaTeX',
        N'Hauptteil der FB 18 Vorlage für Modulbeschreibungen in LaTeX.',
        N'Main part of the FB 18 template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.main');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_DESC_TEX_3', N'FB18_MOD_DESC_TEX_3',
        N'Ende der FB 18 Modulbeschreibung in LaTeX',
        N'End of the FB 18 module description in LaTeX',
        N'Ende der FB 18 Vorlage für Modulbeschreibungen in LaTeX.',
        N'End of the FB 18 template for a module description in LaTeX.', 0,
        N'documents.latex.module-description.end');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_HAND_TEX_1', N'TUDa_MOD_HAND_TEX_1', N'Beginn des TUDa Modulhandbuches in LaTeX',
        N'Begin of the TUDa module handbook in LaTeX',
        N'Beginn der TUDa Vorlage für ein Modulhandbuch in LaTeX.',
        N'Begin of the TUDa template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.begin');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_HAND_TEX_2', N'TUDa_MOD_HAND_TEX_2',
        N'Hauptteil des TUDa Modulhandbuches in LaTeX',
        N'Main part of the TUDa module handbook in LaTeX',
        N'Hauptteil der TUDa Vorlage für ein Modulhanduch in LaTeX.',
        N'Main part of the TUDa template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.main');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'TUDa_MOD_HAND_TEX_3', N'TUDa_MOD_HAND_TEX_3', N'Ende des TUDa Modulhandbuches in LaTeX',
        N'End of the TUDa module handbook in LaTeX',
        N'Ende der TUDa Vorlage für ein Modulhanduch in LaTeX.',
        N'End of the TUDa template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.end');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_HAND_TEX_1', N'FB18_MOD_HAND_TEX_1',
        N'Beginn des FB 18 Modulhandbuches in LaTeX',
        N'Begin of the FB 18 module handbook in LaTeX',
        N'Beginn der FB 18 Vorlage für ein Modulhandbuch in LaTeX.',
        N'Begin of the FB 18 template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.begin');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_HAND_TEX_2', N'FB18_MOD_HAND_TEX_2',
        N'Hauptteil des FB 18 Modulhandbuches in LaTeX',
        N'Main part of the FB 18 module handbook in LaTeX',
        N'Hauptteil der FB 18 Vorlage für ein Modulhanduch in LaTeX.',
        N'Main part of the FB 18 template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.main');
INSERT INTO part_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, language_dependent, keyword)
VALUES (N'FB18_MOD_HAND_TEX_3', N'FB18_MOD_HAND_TEX_3', N'Ende des FB 18 Modulhandbuches in LaTeX',
        N'End of the FB 18 module handbook in LaTeX',
        N'Ende der FB 18 Vorlage für ein Modulhanduch in LaTeX.',
        N'End of the FB 18 template for a module handbook in LaTeX.', 0,
        N'documents.latex.module-handbook.end');

-- Part_value
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Module description: official template of the TUDa from department II (date: senate resolution
% (20.09.2013) v3.0 APB 5)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% https://github.com/tudace/tuda_latex_templates
%%
% !TeX program = lualatex
%%

\documentclass[
    ngerman,
    accentcolor=[[${ACCENT_COLOR}]],
    class=report,
    marginpar=false,
    fontsize=10pt,
	pdfa=[[${PDFA}]]
]{tudapub}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[[${LANGUAGE==''english''} ? ''[main=english, ngerman]'' : ''[english, main=ngerman]'']]{babel}
\usepackage[autostyle]{csquotes}

\usepackage{hologo}

\usepackage{longtable}

%%%%%%%%%%%%%%%%%%%
% Custom
%%%%%%%%%%%%%%%%%%%
\usepackage[locale=[[${LANGUAGE==''english''} ? ''UK'' : ''DE'']], detect-all]{siunitx}
\usepackage{paralist}
\usepackage{datetime}

% Make itemize compact
\usepackage{enumitem}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

% General adjustments
\newlength{\longtablewidth}
\setlength{\longtablewidth}{0.675\linewidth}
\setlength{\parindent}{0pt}

% Custom definitions
\newcommand{\negativeHSpace}{\hspace{-6pt}}
\newcommand{\moduleNameColumn}[1]{\multicolumn{2}{|p{173mm}|}{#1}}
\newcommand{\multiColumnLeft}[1]{\multicolumn{2}{|l|}{#1}}
\newcommand{\emptyCharForIncreasingHeight}{\rule{0pt}{12pt}}
\newcommand{\moduleName}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}
\newcommand{\header}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}

% Custom commands
\newcommand{\ampersand}{\&}

\begin{document}

% Adjustments for the table
\setlength\LTright{0pt}
\setlength{\tabcolsep}{2mm}

\section*{[[${LANGUAGE==''english''} ? ''Module description'' : ''Modulbeschreibung'']]}

', 1, 1);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\begin{longtable}{|p{3mm}|p{166mm}|}
\hline
\moduleNameColumn{\header{[[${LANGUAGE==''english''} ? ''Module name'' : ''Modulname'']]}} \\ % Line break
\moduleNameColumn{\moduleName{[[${MODULE_NAME}]]}} \\ % Line break

\hline
\multiColumnLeft{
\begin{tabular}{p{18mm}|p{27mm}|p{26mm}|p{24mm}|p{27mm}|l}
\negativeHSpace \header{[[${LANGUAGE==''english''} ? ''Module nr.'' : ''Modul Nr.'']]} & \header{[[${LANGUAGE==''english''} ? ''Credit points'' : ''Leistungspunkte'']]} & \header{[[${LANGUAGE==''english''} ? ''Workload'' : ''Arbeitsaufwand'']]} & \header{[[${LANGUAGE==''english''} ? ''Self study'' : ''Selbststudium'']]} & \header{[[${LANGUAGE==''english''} ? ''Module duration'' : ''Moduldauer'']]} & \header{[[${LANGUAGE==''english''} ? ''Module cycle'' : ''Angebotsturnus'']]} \\ % Line break
\negativeHSpace [[${MODULE_NUMBER}]] & \multicolumn{1}{r|}{[[${MODULE_CREDIT_POINTS}]] CP} & \multicolumn{1}{r|}{[[${MODULE_WORKLOAD}]] h} & \multicolumn{1}{r|}{[[${MODULE_SELF_STUDY}]] h} & [[${MODULE_DURATION}]] & [[${MODULE_CYCLE_OFFERED}]]
\end{tabular}
} \\ % Line break

\hline
\multiColumnLeft{
\begin{tabular}{p{79,3mm}|l}
\negativeHSpace \header{[[${LANGUAGE==''english''} ? ''Language'' : ''Sprache'']]} & \header{[[${LANGUAGE==''english''} ? ''Module owner'' : ''Modulverantwortliche Person'']]} \\ % Line break
\negativeHSpace [[${MODULE_LANGUAGE}]] & [[${MODULE_OWNER}]]
\end{tabular}
} \\ % Line break

\hline

\header{1} & \header{[[${LANGUAGE==''english''} ? ''Courses of this module'' : ''Kurse des Moduls'']]} \\ % Line break
\cline{2-2}
& \negativeHSpace \begin{tabular}{p{23mm}|p{62mm}|p{33,5mm}|p{23,1mm}|p{8mm}}
\header{[[${LANGUAGE==''english''} ? ''Course nr.'' : ''Kurs Nr.'']]} & \header{[[${LANGUAGE==''english''} ? ''Course name'' : ''Kursname'']]} & \header{[[${LANGUAGE==''english''} ? ''Workload (CP)'' : ''Arbeitsaufwand (CP)'']]} & \header{[[${LANGUAGE==''english''} ? ''Teaching form'' : ''Lehrform'']]} & \header{[[${LANGUAGE==''english''} ? ''HPW'' : ''SWS'']]} \\ % Line break
\hline
[(${COURSES})]
\end{tabular} \\ % Line break

\hline

\header{2} & \header{[[${LANGUAGE==''english''} ? ''Teaching content'' : ''Lerninhalt'']]} \\ % Line break
& [[${MODULE_TEACHING_CONTENT}]] \\ % Line break
\hline

\header{3} & \header{[[${LANGUAGE==''english''} ? ''Learning objectives'' : ''Qualifikationsziele / Lernergebnisse'']]} \\ % Line break
& [[${MODULE_LEARNING_OBJECTIVES}]] \\ % Line break
\hline

\header{4} & \header{[[${LANGUAGE==''english''} ? ''Prerequisite for participation'' : ''Voraussetzung für die Teilnahme'']]} \\ % Line break
& [[${MODULE_PREREQUISITE_FOR_PARTICIPATION_CUSTOM}]] \\ % Line break
\hline

\header{5} & \header{[[${LANGUAGE==''english''} ? ''Form of examination'' : ''Prüfungsform'']]} \\ % Line break
& [(${MODULE_FORM_OF_EXAMINATION})] \\ % Line break
\hline

\header{6} & \header{[[${LANGUAGE==''english''} ? ''Prerequisite for the award of credit points'' : ''Voraussetzung für die Vergabe von Leistungspunkten'']]} \\ % Line break
& [[${MODULE_PREREQUISITE_FOR_CREDIT_POINTS}]] \\ % Line break
\hline

\header{7} & \header{[[${LANGUAGE==''english''} ? ''Grading'' : ''Benotung'']]} \\ % Line break
& [(${MODULE_GRADING})] \\ % Line break
\hline

\header{8} & \header{[[${LANGUAGE==''english''} ? ''Usability of the module'' : ''Verwendbarkeit des Moduls'']]} \\ % Line break
& [[${MODULE_USABILITY}]] \\ % Line break
\hline

\header{9} & \header{[[${LANGUAGE==''english''} ? ''References'' : ''Literatur'']]} \\ % Line break
& [[${MODULE_REFERENCES}]] \\ % Line break
\hline

\header{10} & \header{[[${LANGUAGE==''english''} ? ''Comment'' : ''Kommentar'']]} \\ % Line break
& [[${MODULE_COMMENT}]] \\ % Line break
\hline

\end{longtable}

', 1, 2);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\end{document}', 1, 3);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Module description template for FB18 - etit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% https://github.com/tudace/tuda_latex_templates
%%
% !TeX program = lualatex
%%

\documentclass[
    ngerman,
    accentcolor=[[${ACCENT_COLOR}]],
    class=report,
    marginpar=false,
    fontsize=10pt,
	pdfa=[[${PDFA}]]
]{tudapub}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[[${LANGUAGE==''english''} ? ''[main=english, ngerman]'' : ''[english, main=ngerman]'']]{babel}
\usepackage[autostyle]{csquotes}

\usepackage{hologo}

\usepackage{longtable}

%%%%%%%%%%%%%%%%%%%
% Custom
%%%%%%%%%%%%%%%%%%%
\usepackage[locale=[[${LANGUAGE==''english''} ? ''UK'' : ''DE'']], detect-all]{siunitx}
\usepackage{paralist}
\usepackage{datetime}

% Make itemize compact
\usepackage{enumitem}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

% General adjustments
\newlength{\longtablewidth}
\setlength{\longtablewidth}{0.675\linewidth}
\setlength{\parindent}{0pt}

% Custom definitions
\newcommand{\negativeHSpace}{\hspace{-6pt}}
\newcommand{\moduleNameColumn}[1]{\multicolumn{2}{|p{173mm}|}{#1}}
\newcommand{\multiColumnLeft}[1]{\multicolumn{2}{|l|}{#1}}
\newcommand{\emptyCharForIncreasingHeight}{\rule{0pt}{12pt}}
\newcommand{\moduleName}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}
\newcommand{\header}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}

\newcommand{\coursesTableBegin}{\begin{tabular}{p{24mm}|p{103mm}|p{32mm}|p{8,9mm}}}
\newcommand{\coursesTableEnd}{\end{tabular}}
\newcommand{\coursesFirstRow}[1]{\multicolumn{3}{p{134mm}}{#1}}
\newcommand{\coursesSecondRow}[1]{\multicolumn{2}{p{119mm}|}{#1}}

% Custom commands
\newcommand{\ampersand}{\&}

\begin{document}

% Adjustments for the table
\setlength\LTright{0pt}
\setlength{\tabcolsep}{2mm}

', 2, 4);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\begin{longtable}{|p{3mm}|p{168mm}|}
\hline
\moduleNameColumn{\header{[[${LANGUAGE==''english''} ? ''Module name'' : ''Modulname'']]}} \\ % Line break
\moduleNameColumn{\moduleName{[[${MODULE_NAME}]]}} \\ % Line break

\hline
\multiColumnLeft{
\begin{tabular}{p{18mm}|p{27mm}|p{26mm}|p{24mm}|p{27mm}|l}
\negativeHSpace \header{[[${LANGUAGE==''english''} ? ''Module nr.'' : ''Modul Nr.'']]} & \header{[[${LANGUAGE==''english''} ? ''Credit points'' : ''Leistungspunkte'']]} & \header{[[${LANGUAGE==''english''} ? ''Workload'' : ''Arbeitsaufwand'']]} & \header{[[${LANGUAGE==''english''} ? ''Self study'' : ''Selbststudium'']]} & \header{[[${LANGUAGE==''english''} ? ''Module duration'' : ''Moduldauer'']]} & \header{[[${LANGUAGE==''english''} ? ''Module cycle'' : ''Angebotsturnus'']]} \\ % Line break
\negativeHSpace [[${MODULE_NUMBER}]] & \multicolumn{1}{r|}{[[${MODULE_CREDIT_POINTS}]] CP} & \multicolumn{1}{r|}{[[${MODULE_WORKLOAD}]] h} & \multicolumn{1}{r|}{[[${MODULE_SELF_STUDY}]] h} & [[${MODULE_DURATION}]] & [[${MODULE_CYCLE_OFFERED}]]
\end{tabular}
} \\ % Line break

\hline
\multiColumnLeft{
\begin{tabular}{p{79,3mm}|l}
\negativeHSpace \header{[[${LANGUAGE==''english''} ? ''Language'' : ''Sprache'']]} & \header{[[${LANGUAGE==''english''} ? ''Module owner'' : ''Modulverantwortliche Person'']]} \\ % Line break
\negativeHSpace [[${MODULE_LANGUAGE}]] & [[${MODULE_OWNER}]]
\end{tabular}
} \\ % Line break

\hline

\header{1} & \header{[[${LANGUAGE==''english''} ? ''Teaching content'' : ''Lerninhalt'']]} \\ % Line break
& [[${MODULE_TEACHING_CONTENT}]] \\ % Line break
\hline

\header{2} & \header{[[${LANGUAGE==''english''} ? ''Learning objectives'' : ''Qualifikationsziele / Lernergebnisse'']]} \\ % Line break
& [[${MODULE_LEARNING_OBJECTIVES}]] \\ % Line break
\hline

\header{3} & \header{[[${LANGUAGE==''english''} ? ''Recommended prerequisites for participation'' : ''Empfohlene Voraussetzungen für die Teilnahme'']]} \\ % Line break
& [[${MODULE_PREREQUISITE_FOR_PARTICIPATION}]] \\ % Line break
\hline

\header{4} & \header{[[${LANGUAGE==''english''} ? ''Form of examination'' : ''Prüfungsform'']]} \\ % Line break
& [(${MODULE_FORM_OF_EXAMINATION})] \\ % Line break
\hline

\header{5} & \header{[[${LANGUAGE==''english''} ? ''Prerequisite for the award of credit points'' : ''Voraussetzung für die Vergabe von Leistungspunkten'']]} \\ % Line break
& [[${MODULE_PREREQUISITE_FOR_CREDIT_POINTS}]] \\ % Line break
\hline

\header{6} & \header{[[${LANGUAGE==''english''} ? ''Grading'' : ''Benotung'']]} \\ % Line break
& [(${MODULE_GRADING})] \\ % Line break
\hline

\header{7} & \header{[[${LANGUAGE==''english''} ? ''Usability of the module'' : ''Verwendbarkeit des Moduls'']]} \\ % Line break
& [[${MODULE_USABILITY}]] \\ % Line break
\hline

\header{8} & \header{[[${LANGUAGE==''english''} ? ''Grade bonus compliant to \S 25 (2)'' : ''Notenverbesserung nach \S 25 (2)'']]} \\ % Line break
& [[${MODULE_GRADE_IMPROVEMENT}]] \\ % Line break
\hline

\header{9} & \header{[[${LANGUAGE==''english''} ? ''References'' : ''Literatur'']]} \\ % Line break
& [[${MODULE_REFERENCES}]] \\ % Line break
\hline


\multiColumnLeft{\header{[[${LANGUAGE==''english''} ? ''Courses'' : ''Enthaltene Kurse'']]}} \\\hline
[(${COURSES_FB18})]

\end{longtable}

', 2, 5);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\end{document}', 2, 6);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Module handbook: official template of the TUDa from department II (date: senate resolution (20.09.2013)
% v3.0 APB 5)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% https://github.com/tudace/tuda_latex_templates
%%
% !TeX program = lualatex
%%

\documentclass[
    ngerman,
    accentcolor=[[${ACCENT_COLOR}]],
    class=report,
    marginpar=false,
    fontsize=10pt,
	pdfa=[[${PDFA}]]
]{tudapub}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[[${LANGUAGE==''english''} ? ''[main=english, ngerman]'' : ''[english, main=ngerman]'']]{babel}
\usepackage[autostyle]{csquotes}

\usepackage{microtype}
\usepackage{hologo}

\usepackage{longtable}
\usepackage{booktabs}

%%%%%%%%%%%%%%%%%%%
% Custom
%%%%%%%%%%%%%%%%%%%
\usepackage[locale=[[${LANGUAGE==''english''} ? ''UK'' : ''DE'']], detect-all]{siunitx}
\usepackage{paralist}
\usepackage{datetime}

% Make itemize compact
\usepackage{enumitem}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

% General adjustments
\newlength{\longtablewidth}
\setlength{\longtablewidth}{0.675\linewidth}
\setlength{\parindent}{0pt}

% Format table of contents
\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}

% Custom definitions
\newcommand{\negativeHSpace}{\hspace{-6pt}}
\newcommand{\moduleNameColumn}[1]{\multicolumn{2}{|p{173mm}|}{#1}}
\newcommand{\multiColumnLeft}[1]{\multicolumn{2}{|l|}{#1}}
\newcommand{\emptyCharForIncreasingHeight}{\rule{0pt}{12pt}}
\newcommand{\moduleName}[1]{\addcontentsline{toc}{section}{#1}#1}
\newcommand{\header}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}

\newcommand{\headingOne}[1]{\chapter{#1}}
\newcommand{\headingTwo}[1]{\section{#1}}
\newcommand{\headingThree}[1]{\subsection{#1}}
\newcommand{\headingFour}[1]{\subsubsection{#1}}
\newcommand{\headingLargerThanFour}[1]{\textbf{#1}}

% Custom commands
\newcommand{\ampersand}{\&}

\begin{document}
% Adjustments for the table
\setlength\LTright{0pt}
\setlength{\tabcolsep}{2mm}

\title{[[${MODULE_HANDBOOK_TITLE}]]}
\subtitle{[[${LANGUAGE==''english''} ? ''Module handbook'' : ''Modulhandbuch'']]}
\author{[[${MODULE_HANDBOOK_AUTHOR}]]}
\date{[[${LANGUAGE==''english''} ? ''Date'' : ''Stand'']]: [[${MODULE_HANDBOOK_DATE}]]}

\addTitleBox{[[${MODULE_HANDBOOK_AUTHOR}]]}

\pagenumbering{Roman}

\maketitle

\clearpage

\begin{tabular}{c}\end{tabular}
\vfill
[[${MODULE_HANDBOOK_ADDITIONAL_TEXT}]] \\ % Line break
~ \\ % Line break
\begin{tabular}{@{}l@{ }l@{}}
	[[${LANGUAGE==''english''} ? ''Module handbook'' : ''Modulhandbuch'']]: & [[${MODULE_HANDBOOK_TITLE}]] \\ % Line break
	&  \\ % Line break
\end{tabular} \\ % Line break
[[${LANGUAGE==''english''} ? ''Date'' : ''Stand'']]: [[${MODULE_HANDBOOK_DATE}]] \\ \\ % Line break
[[${MODULE_HANDBOOK_AUTHOR}]] \\ % Line break
[[${LANGUAGE==''english''} ? ''Email'' : ''Email'']]: [[${MODULE_HANDBOOK_EMAIL}]]

\clearpage
\tableofcontents

\clearpage
\newcounter{savecounter}
\setcounter{savecounter}{\value{page}}
\pagenumbering{arabic}

', 1, 7);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', 1, 8);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\end{document}', 1, 9);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Module handbook template for FB18 - etit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% https://github.com/tudace/tuda_latex_templates
%%
% !TeX program = lualatex
%%

\documentclass[
    ngerman,
    accentcolor=[[${ACCENT_COLOR}]],
    class=report,
    marginpar=false,
    fontsize=10pt,
	pdfa=[[${PDFA}]]
]{tudapub}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[[${LANGUAGE==''english''} ? ''[main=english, ngerman]'' : ''[english, main=ngerman]'']]{babel}
\usepackage[autostyle]{csquotes}

\usepackage{microtype}
\usepackage{hologo}

\usepackage{longtable}
\usepackage{booktabs}

%%%%%%%%%%%%%%%%%%%
% Custom
%%%%%%%%%%%%%%%%%%%
\usepackage[locale=[[${LANGUAGE==''english''} ? ''UK'' : ''DE'']], detect-all]{siunitx}
\usepackage{paralist}
\usepackage{datetime}

% Make itemize compact
\usepackage{enumitem}
\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}
\setenumerate{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt}

% General adjustments
\newlength{\longtablewidth}
\setlength{\longtablewidth}{0.675\linewidth}
\setlength{\parindent}{0pt}

% Format table of contents
\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}

% Custom definitions
\newcommand{\negativeHSpace}{\hspace{-6pt}}
\newcommand{\moduleNameColumn}[1]{\multicolumn{2}{|p{173mm}|}{#1}}
\newcommand{\multiColumnLeft}[1]{\multicolumn{2}{|l|}{#1}}
\newcommand{\emptyCharForIncreasingHeight}{\rule{0pt}{12pt}}
\newcommand{\moduleName}[1]{\addcontentsline{toc}{section}{#1}#1}
\newcommand{\header}[1]{{\emptyCharForIncreasingHeight\textbf{#1}}}

\newcommand{\coursesTableBegin}{\begin{tabular}{p{24mm}|p{103mm}|p{32mm}|p{8,9mm}}}
\newcommand{\coursesTableEnd}{\end{tabular}}
\newcommand{\coursesFirstRow}[1]{\multicolumn{3}{p{134mm}}{#1}}
\newcommand{\coursesSecondRow}[1]{\multicolumn{2}{p{119mm}|}{#1}}

\newcommand{\headingOne}[1]{\chapter{#1}}
\newcommand{\headingTwo}[1]{\section{#1}}
\newcommand{\headingThree}[1]{\subsection{#1}}
\newcommand{\headingFour}[1]{\subsubsection{#1}}
\newcommand{\headingLargerThanFour}[1]{\textbf{#1}}

% Custom commands
\newcommand{\ampersand}{\&}

\begin{document}
% Adjustments for the table
\setlength\LTright{0pt}
\setlength{\tabcolsep}{2mm}

\title{[[${MODULE_HANDBOOK_TITLE}]]}
\subtitle{[[${LANGUAGE==''english''} ? ''Module handbook'' : ''Modulhandbuch'']]}
\author{[[${MODULE_HANDBOOK_AUTHOR}]]}
\date{[[${LANGUAGE==''english''} ? ''Date'' : ''Stand'']]: [[${MODULE_HANDBOOK_DATE}]]}

\addTitleBox{[[${MODULE_HANDBOOK_AUTHOR}]]}

\pagenumbering{Roman}

\maketitle

\clearpage

\begin{tabular}{c}\end{tabular}
\vfill
[[${MODULE_HANDBOOK_ADDITIONAL_TEXT}]] \\ % Line break
~ \\ % Line break
\begin{tabular}{@{}l@{ }l@{}}
	[[${LANGUAGE==''english''} ? ''Module handbook'' : ''Modulhandbuch'']]: & [[${MODULE_HANDBOOK_TITLE}]] \\ % Line break
	&  \\ % Line break
\end{tabular} \\ % Line break
[[${LANGUAGE==''english''} ? ''Date'' : ''Stand'']]: [[${MODULE_HANDBOOK_DATE}]] \\ \\ % Line break
[[${MODULE_HANDBOOK_AUTHOR}]] \\ % Line break
[[${LANGUAGE==''english''} ? ''Email'' : ''Email'']]: [[${MODULE_HANDBOOK_EMAIL}]]

\clearpage
\tableofcontents

\clearpage
\newcounter{savecounter}
\setcounter{savecounter}{\value{page}}
\pagenumbering{arabic}

', 2, 10);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%', 2, 11);
INSERT INTO part_value (content_de, organization_id, key_id)
VALUES (N'
\end{document}', 2, 12);

-- Document_Template_has_Part_Key
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (1, 1);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (1, 2);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (1, 3);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (2, 4);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (2, 5);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (2, 6);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 1);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 2);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 3);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 7);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 8);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (3, 9);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 4);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 5);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 6);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 10);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 11);
INSERT INTO document_template_has_part_key (document_template_id, part_key_id)
VALUES (4, 12);

-- Document_Template_has_File_Format
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (1, 1);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (1, 2);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (2, 1);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (2, 2);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (3, 1);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (3, 2);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (4, 1);
INSERT INTO document_template_has_file_format (document_template_id, file_format_id)
VALUES (4, 2);
