-- DO NOT CHANGE THE ORDER!

USE testvfl;

-- -----------------------------------------------------
-- Create all tables new
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Master data tables without foreign keys except city
-- -----------------------------------------------------
-- Table country
CREATE TABLE country
(
	id           INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	numeric_code NCHAR(3)       NOT NULL UNIQUE,
	alpha_2      NCHAR(2)       NOT NULL UNIQUE,
	alpha_3      NCHAR(3)       NOT NULL UNIQUE,
	name_de      NVARCHAR(200)  NOT NULL UNIQUE,
	name_en      NVARCHAR(200)  NOT NULL UNIQUE,
	created_at   DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at   DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at   DATETIMEOFFSET
);

-- Table city
CREATE TABLE city
(
	id                    INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	subdivision           NCHAR(6)       NOT NULL,
	name_de               NVARCHAR(200)  NOT NULL UNIQUE,
	name_en               NVARCHAR(200)  NOT NULL UNIQUE,
	name_de_wo_diacritics NVARCHAR(200)  NOT NULL UNIQUE,
	name_en_wo_diacritics NVARCHAR(200)  NOT NULL UNIQUE,
	latitude              DECIMAL(10, 8) NOT NULL,
	longitude             DECIMAL(11, 8) NOT NULL,
	country_id            INT            NOT NULL FOREIGN KEY REFERENCES country (id),
	created_at            DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at            DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at            DATETIMEOFFSET
);

-- Table file_format
CREATE TABLE file_format
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table exam_category
CREATE TABLE exam_category
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table exam_type
CREATE TABLE exam_type
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table term
CREATE TABLE term
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table semester
CREATE TABLE semester
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	term_id         INT            NOT NULL FOREIGN KEY REFERENCES term (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table course_type
CREATE TABLE course_type
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table course_category
CREATE TABLE course_category
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table teaching_language
CREATE TABLE teaching_language
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table tile
CREATE TABLE title
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table duration
CREATE TABLE duration
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table rotation
CREATE TABLE rotation
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table grading_system
CREATE TABLE grading_system
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table weighting_method
CREATE TABLE weighting_method
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table document_area
CREATE TABLE document_area
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table responsibility
CREATE TABLE responsibility
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table web_language
CREATE TABLE web_language
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table sex
CREATE TABLE sex
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table external_system
CREATE TABLE external_system
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table external_data
CREATE TABLE external_data
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- -----------------------------------------------------
-- All other tables mostly with foreign keys
-- -----------------------------------------------------

-- Table person_preference
CREATE TABLE person_preference
(
	id                            INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	email_notification_on_message BIT            NOT NULL DEFAULT 1,
	web_language_id               INT            NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES web_language (id),
	created_at                    DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                    DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                    DATETIMEOFFSET
);

-- Table person_profile
CREATE TABLE person_profile
(
	id                      INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	sex_id                  INT            NOT NULL DEFAULT 1 FOREIGN KEY REFERENCES sex (id),
	title_id                INT FOREIGN KEY REFERENCES title (id),
	birth_date              DATE,
	birth_city_id           INT FOREIGN KEY REFERENCES city (id),
	private_address_street  NVARCHAR(200),
	private_address_city_id INT FOREIGN KEY REFERENCES city (id),
	private_email           NVARCHAR(200),
	private_telephone       NVARCHAR(20),
	private_mobile          NVARCHAR(20),
	private_fax             NVARCHAR(20),
	business_email          NVARCHAR(200),
	business_telephone      NVARCHAR(20),
	business_mobile         NVARCHAR(20),
	business_fax            NVARCHAR(20),
	default_email           NVARCHAR(200),
	created_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at              DATETIMEOFFSET
);

-- Table person
CREATE TABLE person
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	email          NVARCHAR(200),
	first_name     NVARCHAR(200)  NOT NULL,
	middle_name    NVARCHAR(200),
	last_name      NVARCHAR(200)  NOT NULL,
	description_de NVARCHAR(MAX),
	description_en NVARCHAR(MAX),
	last_verified  DATETIMEOFFSET,
	preference_id  INT            NOT NULL FOREIGN KEY REFERENCES person_preference (id),
	profile_id     INT            NOT NULL FOREIGN KEY REFERENCES person_profile (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET,
);

-- Table account_local
CREATE TABLE account_local
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	user_name      NVARCHAR(200)  NOT NULL UNIQUE,
	password       NVARCHAR(200)  NOT NULL,
	active         BIT                     DEFAULT 0,
	description_de NVARCHAR(MAX),
	description_en NVARCHAR(MAX),
	last_login     DATETIMEOFFSET,
	person_id      INT            NOT NULL FOREIGN KEY REFERENCES person (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET,
	CONSTRAINT CK_AccountLocal_CheckUserNameNotEmpty CHECK (user_name <> ''),
	CONSTRAINT CK_AccountLocal_CheckPasswordNotEmpty CHECK (password <> '')
);

-- Table account_tuda
CREATE TABLE account_tuda
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	tu_id          NVARCHAR(8)    NOT NULL UNIQUE,
	active         BIT                     DEFAULT 0,
	description_de NVARCHAR(MAX),
	description_en NVARCHAR(MAX),
	last_verified  DATETIMEOFFSET,
	last_login     DATETIMEOFFSET,
	person_id      INT            NOT NULL UNIQUE FOREIGN KEY REFERENCES person (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET,
	CONSTRAINT CK_AccountTuda_CheckTuidNotEmpty CHECK (tu_id <> '')
);

-- Table organization
CREATE TABLE organization
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	cost_center     NVARCHAR(20),
	parent_id       INT FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table organization_has_person
CREATE TABLE organization_has_person
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	person_id       INT            NOT NULL FOREIGN KEY REFERENCES person (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT UQ_OrganizationHasPerson_CheckCombinationIsUnique UNIQUE (organization_id, person_id)
);

-- Table role
CREATE TABLE role
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	protected       BIT,
	uuid            NVARCHAR(200)  NOT NULL UNIQUE,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	organization_id INT FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table role_has_role
CREATE TABLE role_has_role
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	parent_role_id INT            NOT NULL FOREIGN KEY REFERENCES ROLE (id),
	child_role_id  INT            NOT NULL FOREIGN KEY REFERENCES ROLE (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET
);


-- Table account_has_role
CREATE TABLE account_has_role
(
	id               INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	role_id          INT            NOT NULL FOREIGN KEY REFERENCES ROLE (id),
	account_local_id INT FOREIGN KEY REFERENCES account_local (id),
	account_tuda_id  INT FOREIGN KEY REFERENCES account_tuda (id),
	created_at       DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at       DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at       DATETIMEOFFSET
);

-- Table permission
CREATE TABLE permission
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	uuid            NVARCHAR(200)  NOT NULL UNIQUE,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table resource
CREATE TABLE resource
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	uuid            NVARCHAR(200)  NOT NULL UNIQUE,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200),
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table permission_has_resource
CREATE TABLE permission_has_resource
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	permission_id   INT            NOT NULL FOREIGN KEY REFERENCES permission (id),
	resource_id     INT            NOT NULL FOREIGN KEY REFERENCES resource (id),
	organization_id INT FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table role_has_permission_and_resource
CREATE TABLE role_has_permission_and_resource
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	role_id         INT            NOT NULL FOREIGN KEY REFERENCES role (id),
	permission_id   INT            NOT NULL FOREIGN KEY REFERENCES permission (id),
	resource_id     INT            NOT NULL FOREIGN KEY REFERENCES resource (id),
	organization_id INT FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table module_base
CREATE TABLE module_base
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	tucan_module_id BIGINT         NOT NULL UNIQUE,
	activated_at    DATETIMEOFFSET,
	deactivated_at  DATETIMEOFFSET,
	organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT CK_ModuleBase_CheckTucanModuleIdLargerThanZero CHECK (tucan_module_id > 0)
);

-- Table module_detail
CREATE TABLE module_detail
(
	id                            INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number                        NVARCHAR(20)   NOT NULL,
	abbreviation_de               NVARCHAR(20),
	abbreviation_en               NVARCHAR(20),
	certificate_abbreviation_de   NVARCHAR(20),
	certificate_abbreviation_en   NVARCHAR(20),
	name_de                       NVARCHAR(200)  NOT NULL,
	name_en                       NVARCHAR(200),
	description_de                NVARCHAR(MAX),
	description_en                NVARCHAR(MAX),
	credit_points                 TINYINT        NOT NULL,
	teaching_content_de           NVARCHAR(MAX),
	teaching_content_en           NVARCHAR(MAX),
	learning_objectives_de        NVARCHAR(MAX),
	learning_objectives_en        NVARCHAR(MAX),
	references_de                 NVARCHAR(MAX),
	references_en                 NVARCHAR(MAX),
	option_de                     NVARCHAR(MAX),
	option_en                     NVARCHAR(MAX),
	usability_de                  NVARCHAR(MAX),
	usability_en                  NVARCHAR(MAX),
	prerequisite_participation_de NVARCHAR(MAX),
	prerequisite_participation_en NVARCHAR(MAX),
	prerequisite_credit_points_de NVARCHAR(MAX),
	prerequisite_credit_points_en NVARCHAR(MAX),
	grade_improvement_de          NVARCHAR(MAX),
	grade_improvement_en          NVARCHAR(MAX),
	diploma_supplement_de         NVARCHAR(MAX),
	diploma_supplement_en         NVARCHAR(MAX),
	supplement_form_exam_de       NVARCHAR(MAX),
	supplement_form_exam_en       NVARCHAR(MAX),
	comment_de                    NVARCHAR(MAX),
	comment_en                    NVARCHAR(MAX),
	teaching_language_id          INT            NOT NULL FOREIGN KEY REFERENCES teaching_language (id),
	duration_id                   INT            NOT NULL FOREIGN KEY REFERENCES duration (id),
	rotation_id                   INT            NOT NULL FOREIGN KEY REFERENCES rotation (id),
	grading_system_id             INT            NOT NULL FOREIGN KEY REFERENCES grading_system (id),
	weighting_method_id           INT            NOT NULL FOREIGN KEY REFERENCES weighting_method (id),
	created_at                    DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                    DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                    DATETIMEOFFSET
);

-- Table module_version
CREATE TABLE module_version
(
	id                      INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number                  TINYINT        NOT NULL,
	activated_at            DATETIMEOFFSET,
	deactivated_at          DATETIMEOFFSET,
	name_de                 NVARCHAR(200),
	name_en                 NVARCHAR(200),
	description_de          NVARCHAR(MAX),
	description_en          NVARCHAR(MAX),
	detail_id               INT            NOT NULL FOREIGN KEY REFERENCES module_detail (id),
	tucan_module_version_id BIGINT,
	created_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at              DATETIMEOFFSET,
	CONSTRAINT CK_ModuleVersion_CheckTucanModuleVersionIdNullOrLargerThanZero CHECK ((tucan_module_version_id IS NULL) OR
	                                                                                 (tucan_module_version_id > 0))
);

-- Table module_base_has_module_version
CREATE TABLE module_base_has_module_version
(
	id         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	base_id    INT            NOT NULL FOREIGN KEY REFERENCES module_base (id),
	version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
	created_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at DATETIMEOFFSET,
	CONSTRAINT UQ_ModuleBaseHasModuleVersion_CheckCombinationIsUnique UNIQUE (base_id, version_id)
);

-- Table module_version_has_responsible_person
CREATE TABLE module_version_has_responsible_person
(
	id                INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	module_version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
	person_id         INT            NOT NULL FOREIGN KEY REFERENCES person (id),
	responsibility_id INT            NOT NULL FOREIGN KEY REFERENCES responsibility (id),
	created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at        DATETIMEOFFSET,
	CONSTRAINT UQ_ModuleVersionHasResponsiblePerson_CheckCombinationIsUnique UNIQUE (module_version_id, person_id, responsibility_id)
);

-- Table course_base
CREATE TABLE course_base
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	tucan_course_id BIGINT         NOT NULL UNIQUE,
	activated_at    DATETIMEOFFSET,
	deactivated_at  DATETIMEOFFSET,
	organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT CK_CourseBase_CheckTucanCourseIdtLargerThanZero CHECK (tucan_course_id > 0)
);

-- Table course_detail
CREATE TABLE course_detail
(
	id                          INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number                      NVARCHAR(20)   NOT NULL,
	abbreviation_de             NVARCHAR(20),
	abbreviation_en             NVARCHAR(20),
	certificate_abbreviation_de NVARCHAR(20),
	certificate_abbreviation_en NVARCHAR(20),
	name_de                     NVARCHAR(200)  NOT NULL,
	name_en                     NVARCHAR(200),
	contact_hours_per_week      TINYINT        NOT NULL,
	category_id                 INT            NOT NULL FOREIGN KEY REFERENCES course_category (id),
	type_id                     INT FOREIGN KEY REFERENCES course_type (id),
	teaching_language_id        INT FOREIGN KEY REFERENCES teaching_language (id),
	created_at                  DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                  DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                  DATETIMEOFFSET
);

-- Table course_version
CREATE TABLE course_version
(
	id                      INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number                  TINYINT        NOT NULL,
	activated_at            DATETIMEOFFSET,
	deactivated_at          DATETIMEOFFSET,
	name_de                 NVARCHAR(200),
	name_en                 NVARCHAR(200),
	description_de          NVARCHAR(MAX),
	description_en          NVARCHAR(MAX),
	detail_id               INT            NOT NULL FOREIGN KEY REFERENCES course_detail (id),
	tucan_course_version_id BIGINT,
	created_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at              DATETIMEOFFSET,
	CONSTRAINT CK_CourseVersion_CheckTucanCourseVersionIdNullOrLargerThanZero CHECK ((tucan_course_version_id IS NULL) OR
	                                                                                 (tucan_course_version_id > 0))

);

-- Table course_base_has_course_version
CREATE TABLE course_base_has_course_version
(
	id         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	base_id    INT            NOT NULL FOREIGN KEY REFERENCES course_base (id),
	version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
	created_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at DATETIMEOFFSET,
	CONSTRAINT UQ_CourseBaseHasCourseVersion_CheckCombinationIsUnique UNIQUE (base_id, version_id)
);

-- Table course_version_has_responsible_person
CREATE TABLE course_version_has_responsible_person
(
	id                INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	course_version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
	person_id         INT            NOT NULL FOREIGN KEY REFERENCES person (id),
	responsibility_id INT            NOT NULL FOREIGN KEY REFERENCES responsibility (id),
	created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at        DATETIMEOFFSET,
	CONSTRAINT UQ_CourseVersionHasResponsiblePerson_CheckCombinationIsUnique UNIQUE (course_version_id, person_id, responsibility_id)
);

-- Table module_version_has_course_version
CREATE TABLE module_version_has_course_version
(
	id                INT            NOT NULL IDENTITY ( 1, 1 ) PRIMARY KEY,
	module_version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
	course_version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
	created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at        DATETIMEOFFSET,
	CONSTRAINT UQ_ModuleVersionHasCourseVersion_CheckCombinationIsUnique UNIQUE (module_version_id, course_version_id)
);

-- Table exam_base
CREATE TABLE exam_base
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	tucan_module_id BIGINT         NOT NULL,
	tucan_course_id BIGINT,
	activated_at    DATETIMEOFFSET,
	deactivated_at  DATETIMEOFFSET,
	organization_id INTEGER        NOT NULL FOREIGN KEY REFERENCES organization (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT CK_ExamBase_CheckTucanModuleIdNullOrLargerThanZero CHECK ((tucan_module_id IS NULL) OR (tucan_module_id > 0)),
	CONSTRAINT CK_ExamBase_CheckTucanCourseIdNullOrLargerThanZero CHECK ((tucan_course_id IS NULL) OR (tucan_course_id > 0))

);

-- Table exam_detail
CREATE TABLE exam_detail
(
	id                       INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number                   NVARCHAR(20)   NOT NULL,
	name_de                  NVARCHAR(200)  NOT NULL,
	name_en                  NVARCHAR(200),
	mandatory_to_pass        BIT            NOT NULL,
	duration                 TINYINT        NOT NULL,
	mark_included_in_grading BIT            NOT NULL,
	weighting_of_mark        SMALLINT       NOT NULL,
	explicit_registration    BIT            NOT NULL,
	category_id              INT            NOT NULL FOREIGN KEY REFERENCES exam_category (id),
	type_id                  INT            NOT NULL FOREIGN KEY REFERENCES exam_type (id),
	grading_system_id        INT            NOT NULL FOREIGN KEY REFERENCES grading_system (id),
	term_id                  INT FOREIGN KEY REFERENCES term (id),
	created_at               DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at               DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at               DATETIMEOFFSET
);

-- Table exam_version
CREATE TABLE exam_version
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	number         TINYINT        NOT NULL,
	activated_at   DATETIMEOFFSET,
	deactivated_at DATETIMEOFFSET,
	name_de        NVARCHAR(200),
	name_en        NVARCHAR(200),
	description_de NVARCHAR(MAX),
	description_en NVARCHAR(MAX),
	detail_id      INT            NOT NULL FOREIGN KEY REFERENCES exam_detail (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET
);

-- Table exam_base_has_exam_version
CREATE TABLE exam_base_has_exam_version
(
	id         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	base_id    INT            NOT NULL FOREIGN KEY REFERENCES exam_base (id),
	version_id INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
	created_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at DATETIMEOFFSET,
	CONSTRAINT UQ_ExamBaseHasExamVersion_CheckCombinationIsUnique UNIQUE (base_id, version_id)
);


-- Table exam_version_has_tucan_exam_id
CREATE TABLE exam_version_has_tucan_exam_id
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	exam_version_id INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
	tucan_exam_id   BIGINT         NOT NULL,
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT CK_ExamVersionHasTucanExamId_CheckTucanExamIdLargerThanZero CHECK (tucan_exam_id > 0),
	CONSTRAINT UQ_ExamVersionHasTucanExamId_CheckCombinationIsUnique UNIQUE (exam_version_id, tucan_exam_id)
);

-- Table exam_version_has_responsible_person
CREATE TABLE exam_version_has_responsible_person
(
	id                INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	exam_version_id   INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
	person_id         INT            NOT NULL FOREIGN KEY REFERENCES person (id),
	responsibility_id INT            NOT NULL FOREIGN KEY REFERENCES responsibility (id),
	created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at        DATETIMEOFFSET,
	CONSTRAINT UQ_ExamVersionHasResponsiblePerson_CheckCombinationIsUnique UNIQUE (exam_version_id, person_id, responsibility_id)
);

-- Table module_version_has_exam_version
CREATE TABLE module_version_has_exam_version
(
	id                INT            NOT NULL IDENTITY ( 1, 1 ) PRIMARY KEY,
	module_version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
	exam_version_id   INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
	created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at        DATETIMEOFFSET,
	CONSTRAINT UQ_ModuleVersionHasExamVersion_CheckCombinationIsUnique UNIQUE (module_version_id, exam_version_id)
);

-- Table document_category
CREATE TABLE document_category
(
	id               INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de  NVARCHAR(20),
	abbreviation_en  NVARCHAR(20),
	name_de          NVARCHAR(200)  NOT NULL UNIQUE,
	name_en          NVARCHAR(200)  NOT NULL UNIQUE,
	description_de   NVARCHAR(MAX),
	description_en   NVARCHAR(MAX),
	document_area_id INT            NOT NULL FOREIGN KEY REFERENCES document_area (id),
	created_at       DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at       DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at       DATETIMEOFFSET
);

-- Table document_type
CREATE TABLE document_type
(
	id                         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de            NVARCHAR(20),
	abbreviation_en            NVARCHAR(20),
	name_de                    NVARCHAR(200)  NOT NULL UNIQUE,
	name_en                    NVARCHAR(200)  NOT NULL UNIQUE,
	description_de             NVARCHAR(MAX),
	description_en             NVARCHAR(MAX),
	document_category_id       INT            NOT NULL FOREIGN KEY REFERENCES document_category (id),
	belongs_to_organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	created_at                 DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                 DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                 DATETIMEOFFSET
);

-- Table document_template
CREATE TABLE document_template
(
	id                         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de            NVARCHAR(20),
	abbreviation_en            NVARCHAR(20),
	name_de                    NVARCHAR(200)  NOT NULL UNIQUE,
	name_en                    NVARCHAR(200)  NOT NULL UNIQUE,
	description_de             NVARCHAR(MAX),
	description_en             NVARCHAR(MAX),
	document_type_id           INT            NOT NULL FOREIGN KEY REFERENCES document_type (id),
	belongs_to_organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	created_at                 DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                 DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                 DATETIMEOFFSET
);

-- Table document_template_has_template
CREATE TABLE document_template_has_template
(
	id                 INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	parent_template_id INT            NOT NULL FOREIGN KEY REFERENCES document_template (id),
	child_template_id  INT            NOT NULL FOREIGN KEY REFERENCES document_template (id),
	created_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at         DATETIMEOFFSET,
	CONSTRAINT UQ_DocumentTemplateHasTemplate_CheckCombinationIsUnique UNIQUE (parent_template_id, child_template_id)
);

-- Table part_key
CREATE TABLE part_key
(
	id                 INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de    NVARCHAR(20),
	abbreviation_en    NVARCHAR(20),
	name_de            NVARCHAR(200)  NOT NULL UNIQUE,
	name_en            NVARCHAR(200)  NOT NULL UNIQUE,
	description_de     NVARCHAR(MAX),
	description_en     NVARCHAR(MAX),
	language_dependent BIT            NOT NULL,
	keyword            NVARCHAR(200)  NOT NULL,
	created_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at         DATETIMEOFFSET
);

-- Table part_value
CREATE TABLE part_value
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	content_de      NVARCHAR(MAX)  NOT NULL,
	content_en      NVARCHAR(MAX),
	organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	key_id          INT            NOT NULL FOREIGN KEY REFERENCES part_key (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table document_template_has_part_key
CREATE TABLE document_template_has_part_key
(
	id                   INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	document_template_id INT            NOT NULL FOREIGN KEY REFERENCES document_template (id),
	part_key_id          INT            NOT NULL FOREIGN KEY REFERENCES part_key (id),
	created_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at           DATETIMEOFFSET,
	CONSTRAINT UQ_DocumentTemplateHasPartKey_CheckCombinationIsUnique UNIQUE (document_template_id, part_key_id)
);

-- Table document_template_has_file_format
CREATE TABLE document_template_has_file_format
(
	id                   INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	document_template_id INT            NOT NULL FOREIGN KEY REFERENCES document_template (id),
	file_format_id       INT            NOT NULL FOREIGN KEY REFERENCES file_format (id),
	created_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at           DATETIMEOFFSET,
	CONSTRAINT UQ_DocumentTemplateHasFileFormat_CheckCombinationIsUnique UNIQUE (document_template_id, file_format_id)
);

-- Table setting_key
CREATE TABLE setting_key
(
	id                 INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de    NVARCHAR(20),
	abbreviation_en    NVARCHAR(20),
	name_de            NVARCHAR(200)  NOT NULL UNIQUE,
	name_en            NVARCHAR(200)  NOT NULL,
	description_de     NVARCHAR(MAX),
	description_en     NVARCHAR(MAX),
	language_dependent BIT            NOT NULL,
	keyword            NVARCHAR(200)  NOT NULL,
	created_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at         DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at         DATETIMEOFFSET
);

-- Table setting_value
CREATE TABLE setting_value
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	content_de      NVARCHAR(MAX)  NOT NULL,
	content_en      NVARCHAR(MAX),
	organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
	key_id          INT            NOT NULL FOREIGN KEY REFERENCES setting_key (id),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET,
	CONSTRAINT UQ_SettingValue_CheckCombinationIsUnique UNIQUE (organization_id, key_id)
);

-- Table document_template_has_setting_key
CREATE TABLE document_template_has_setting_key
(
	id                   INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	document_template_id INT            NOT NULL FOREIGN KEY REFERENCES document_template (id),
	setting_key_id       INT            NOT NULL FOREIGN KEY REFERENCES setting_key (id),
	organization_id      INT FOREIGN KEY REFERENCES organization (id),
	created_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at           DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at           DATETIMEOFFSET,
	CONSTRAINT UQ_DocumentTemplateHasSettingKey_CheckCombinationIsUnique UNIQUE (document_template_id, setting_key_id, organization_id)
);

-- Table feature
CREATE TABLE feature
(
	id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	abbreviation_de NVARCHAR(20),
	abbreviation_en NVARCHAR(20),
	name_de         NVARCHAR(200)  NOT NULL UNIQUE,
	name_en         NVARCHAR(200)  NOT NULL,
	description_de  NVARCHAR(MAX),
	description_en  NVARCHAR(MAX),
	created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at      DATETIMEOFFSET
);

-- Table feature_has_setting_key
CREATE TABLE feature_has_setting_key
(
	id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	feature_id     INT            NOT NULL FOREIGN KEY REFERENCES feature (id),
	setting_key_id INT            NOT NULL FOREIGN KEY REFERENCES setting_key (id),
	created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at     DATETIMEOFFSET,
	CONSTRAINT UQ_FeatureHasSettingKey_CheckCombinationIsUnique UNIQUE (feature_id, setting_key_id)
);

-- Table data_import_log
CREATE TABLE data_import_log
(
	id                        INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
	successful                BIT            NOT NULL,
	started_at                DATETIMEOFFSET NOT NULL,
	finished_at               DATETIMEOFFSET NOT NULL,
	final_result_de           NVARCHAR(MAX)  NOT NULL,
	final_result_en           NVARCHAR(MAX)  NOT NULL,
	info_message              NVARCHAR(MAX),
	warn_message              NVARCHAR(MAX),
	error_message             NVARCHAR(MAX),
	debug_message             NVARCHAR(MAX),
	from_external_system_id   INT            NOT NULL FOREIGN KEY REFERENCES external_system (id),
	imported_external_data_id INT            NOT NULL FOREIGN KEY REFERENCES external_data (id),
	created_at                DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	updated_at                DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
	deleted_at                DATETIMEOFFSET
);