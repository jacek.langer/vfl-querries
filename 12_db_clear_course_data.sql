-- once uid is implemented this can be changed to simple DELETE FROM <table_name> Where id>0

-- This query is needed to reset the id of the identity to 0 as using
-- delete with CHKIDENT proves to be more work for the same result.
USE testvfl;

DROP TABLE IF EXISTS module_version_has_course_version;
DROP TABLE IF EXISTS course_version_has_responsible_person;
DROP TABLE IF EXISTS course_base_has_course_version;
DROP TABLE IF EXISTS course_version;
DROP TABLE IF EXISTS course_detail;
DROP TABLE IF EXISTS course_base;

-- Table course_base
CREATE TABLE course_base
(
    id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    tucan_course_id BIGINT         NOT NULL UNIQUE,
    activated_at    DATETIMEOFFSET,
    deactivated_at  DATETIMEOFFSET,
    organization_id INT            NOT NULL FOREIGN KEY REFERENCES organization (id),
    created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at      DATETIMEOFFSET,
    CONSTRAINT CK_CourseBase_CheckTucanCourseIdtLargerThanZero CHECK (tucan_course_id > 0)
);

-- Table course_detail
CREATE TABLE course_detail
(
    id                          INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    number                      NVARCHAR(20)   NOT NULL,
    abbreviation_de             NVARCHAR(20),
    abbreviation_en             NVARCHAR(20),
    certificate_abbreviation_de NVARCHAR(20),
    certificate_abbreviation_en NVARCHAR(20),
    name_de                     NVARCHAR(200)  NOT NULL,
    name_en                     NVARCHAR(200),
    contact_hours_per_week      TINYINT        NOT NULL,
    category_id                 INT            NOT NULL FOREIGN KEY REFERENCES course_category (id),
    type_id                     INT FOREIGN KEY REFERENCES course_type (id),
    teaching_language_id        INT FOREIGN KEY REFERENCES teaching_language (id),
    created_at                  DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at                  DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at                  DATETIMEOFFSET
);

-- Table course_version
CREATE TABLE course_version
(
    id                      INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    number                  TINYINT        NOT NULL,
    activated_at            DATETIMEOFFSET,
    deactivated_at          DATETIMEOFFSET,
    name_de                 NVARCHAR(200),
    name_en                 NVARCHAR(200),
    description_de          NVARCHAR(MAX),
    description_en          NVARCHAR(MAX),
    detail_id               INT            NOT NULL FOREIGN KEY REFERENCES course_detail (id),
    tucan_course_version_id BIGINT,
    created_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at              DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at              DATETIMEOFFSET,
    CONSTRAINT CK_CourseVersion_CheckTucanCourseVersionIdNullOrLargerThanZero CHECK ((tucan_course_version_id IS NULL) OR
                                                                                     (tucan_course_version_id > 0))

);

-- Table course_base_has_course_version
CREATE TABLE course_base_has_course_version
(
    id         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    base_id    INT            NOT NULL FOREIGN KEY REFERENCES course_base (id),
    version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
    created_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at DATETIMEOFFSET,
    CONSTRAINT UQ_CourseBaseHasCourseVersion_CheckCombinationIsUnique UNIQUE (base_id, version_id)
);

-- Table course_version_has_responsible_person
CREATE TABLE course_version_has_responsible_person
(
    id                INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    course_version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
    person_id         INT            NOT NULL FOREIGN KEY REFERENCES person (id),
    responsibility_id INT            NOT NULL FOREIGN KEY REFERENCES responsibility (id),
    created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at        DATETIMEOFFSET,
    CONSTRAINT UQ_CourseVersionHasResponsiblePerson_CheckCombinationIsUnique UNIQUE (course_version_id, person_id, responsibility_id)
);

-- Table module_version_has_course_version
CREATE TABLE module_version_has_course_version
(
    id                INT            NOT NULL IDENTITY ( 1, 1 ) PRIMARY KEY,
    module_version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
    course_version_id INT            NOT NULL FOREIGN KEY REFERENCES course_version (id),
    created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at        DATETIMEOFFSET,
    CONSTRAINT UQ_ModuleVersionHasCourseVersion_CheckCombinationIsUnique UNIQUE (module_version_id, course_version_id)
);
