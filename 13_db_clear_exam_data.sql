-- once uid is implemented this can be changed to simple DELETE FROM <table_name> Where id>0

-- This query is needed to reset the id of the identity to 0 as using
-- delete with CHKIDENT proves to be more work for the same result.
USE testvfl;

DROP TABLE IF EXISTS module_version_has_exam_version;
DROP TABLE IF EXISTS exam_version_has_responsible_person;
DROP TABLE IF EXISTS exam_version_has_tucan_exam_id;
DROP TABLE IF EXISTS exam_base_has_exam_version;
DROP TABLE IF EXISTS exam_version;
DROP TABLE IF EXISTS exam_detail;
DROP TABLE IF EXISTS exam_base;

-- Table exam_base
CREATE TABLE exam_base
(
    id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    tucan_module_id BIGINT         NOT NULL,
    tucan_course_id BIGINT,
    activated_at    DATETIMEOFFSET,
    deactivated_at  DATETIMEOFFSET,
    organization_id INTEGER        NOT NULL FOREIGN KEY REFERENCES organization (id),
    created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at      DATETIMEOFFSET,
    CONSTRAINT CK_ExamBase_CheckTucanModuleIdNullOrLargerThanZero CHECK ((tucan_module_id IS NULL) OR (tucan_module_id > 0)),
    CONSTRAINT CK_ExamBase_CheckTucanCourseIdNullOrLargerThanZero CHECK ((tucan_course_id IS NULL) OR (tucan_course_id > 0))

);

-- Table exam_detail
CREATE TABLE exam_detail
(
    id                       INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    number                   NVARCHAR(20)   NOT NULL,
    name_de                  NVARCHAR(200)  NOT NULL,
    name_en                  NVARCHAR(200),
    mandatory_to_pass        BIT            NOT NULL,
    duration                 TINYINT        NOT NULL,
    mark_included_in_grading BIT            NOT NULL,
    weighting_of_mark        SMALLINT       NOT NULL,
    explicit_registration    BIT            NOT NULL,
    category_id              INT            NOT NULL FOREIGN KEY REFERENCES exam_category (id),
    type_id                  INT            NOT NULL FOREIGN KEY REFERENCES exam_type (id),
    grading_system_id        INT            NOT NULL FOREIGN KEY REFERENCES grading_system (id),
    term_id                  INT FOREIGN KEY REFERENCES term (id),
    created_at               DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at               DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at               DATETIMEOFFSET
);

-- Table exam_version
CREATE TABLE exam_version
(
    id             INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    number         TINYINT        NOT NULL,
    activated_at   DATETIMEOFFSET,
    deactivated_at DATETIMEOFFSET,
    name_de        NVARCHAR(200),
    name_en        NVARCHAR(200),
    description_de NVARCHAR(MAX),
    description_en NVARCHAR(MAX),
    detail_id      INT            NOT NULL FOREIGN KEY REFERENCES exam_detail (id),
    created_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at     DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at     DATETIMEOFFSET
);

-- Table exam_base_has_exam_version
CREATE TABLE exam_base_has_exam_version
(
    id         INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    base_id    INT            NOT NULL FOREIGN KEY REFERENCES exam_base (id),
    version_id INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
    created_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at DATETIMEOFFSET,
    CONSTRAINT UQ_ExamBaseHasExamVersion_CheckCombinationIsUnique UNIQUE (base_id, version_id)
);


-- Table exam_version_has_tucan_exam_id
CREATE TABLE exam_version_has_tucan_exam_id
(
    id              INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    exam_version_id INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
    tucan_exam_id   BIGINT         NOT NULL,
    created_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at      DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at      DATETIMEOFFSET,
    CONSTRAINT CK_ExamVersionHasTucanExamId_CheckTucanExamIdLargerThanZero CHECK (tucan_exam_id > 0),
    CONSTRAINT UQ_ExamVersionHasTucanExamId_CheckCombinationIsUnique UNIQUE (exam_version_id, tucan_exam_id)
);

-- Table exam_version_has_responsible_person
CREATE TABLE exam_version_has_responsible_person
(
    id                INT            NOT NULL IDENTITY (1,1) PRIMARY KEY,
    exam_version_id   INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
    person_id         INT            NOT NULL FOREIGN KEY REFERENCES person (id),
    responsibility_id INT            NOT NULL FOREIGN KEY REFERENCES responsibility (id),
    created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at        DATETIMEOFFSET,
    CONSTRAINT UQ_ExamVersionHasResponsiblePerson_CheckCombinationIsUnique UNIQUE (exam_version_id, person_id, responsibility_id)
);

-- Table module_version_has_exam_version
CREATE TABLE module_version_has_exam_version
(
    id                INT            NOT NULL IDENTITY ( 1, 1 ) PRIMARY KEY,
    module_version_id INT            NOT NULL FOREIGN KEY REFERENCES module_version (id),
    exam_version_id   INT            NOT NULL FOREIGN KEY REFERENCES exam_version (id),
    created_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    updated_at        DATETIMEOFFSET NOT NULL DEFAULT SYSDATETIMEOFFSET(),
    deleted_at        DATETIMEOFFSET,
    CONSTRAINT UQ_ModuleVersionHasExamVersion_CheckCombinationIsUnique UNIQUE (module_version_id, exam_version_id)
);
