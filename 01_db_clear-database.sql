-- DO NOT CHANGE THE ORDER!

USE
testvfl;

----------------------------------
-- Delete all tables and triggers
----------------------------------

-- Drop all triggers

-- Master data triggers
DROP TRIGGER IF EXISTS country_updated_at;
DROP TRIGGER IF EXISTS city_updated_at;
DROP TRIGGER IF EXISTS file_format_updated_at;
DROP TRIGGER IF EXISTS exam_category_updated_at;
DROP TRIGGER IF EXISTS exam_type_updated_at;
DROP TRIGGER IF EXISTS term_updated_at;
DROP TRIGGER IF EXISTS semester_updated_at;
DROP TRIGGER IF EXISTS course_type_updated_at;
DROP TRIGGER IF EXISTS course_category_updated_at;
DROP TRIGGER IF EXISTS teaching_language_updated_at;
DROP TRIGGER IF EXISTS title_updated_at;
DROP TRIGGER IF EXISTS duration_updated_at;
DROP TRIGGER IF EXISTS rotation_updated_at;
DROP TRIGGER IF EXISTS grading_system_updated_at;
DROP TRIGGER IF EXISTS weighting_method_updated_at;
DROP TRIGGER IF EXISTS document_area_updated_at;
DROP TRIGGER IF EXISTS responsibility_updated_at;
DROP TRIGGER IF EXISTS permission_updated_at;
DROP TRIGGER IF EXISTS resource_updated_at;
DROP TRIGGER IF EXISTS web_lanuage_updated_at;
DROP TRIGGER IF EXISTS sex_updated_at;
DROP TRIGGER IF EXISTS external_system_updated_at;
DROP TRIGGER IF EXISTS external_data_updated_at;

-- Other triggers
DROP TRIGGER IF EXISTS person_preference_updated_at;
DROP TRIGGER IF EXISTS person_profile_updated_at;
DROP TRIGGER IF EXISTS person_updated_at;
DROP TRIGGER IF EXISTS account_local_updated_at;
DROP TRIGGER IF EXISTS account_tuda_updated_at;
DROP TRIGGER IF EXISTS organization_updated_at;
DROP TRIGGER IF EXISTS organization_has_person_updated_at;
DROP TRIGGER IF EXISTS role_updated_at;
DROP TRIGGER IF EXISTS role_has_role_updated_at;
DROP TRIGGER IF EXISTS account_has_role_updated_at;
DROP TRIGGER IF EXISTS permission_has_resource_updated_at;
DROP TRIGGER IF EXISTS role_has_permission_and_resource_updated_at;
DROP TRIGGER IF EXISTS module_base_updated_at;
DROP TRIGGER IF EXISTS module_detail_updated_at;
DROP TRIGGER IF EXISTS module_version_updated_at;
DROP TRIGGER IF EXISTS module_base_has_module_version_updated_at;
DROP TRIGGER IF EXISTS module_version_has_responsible_person_updated_at;
DROP TRIGGER IF EXISTS course_base_updated_at;
DROP TRIGGER IF EXISTS course_detail_updated_at;
DROP TRIGGER IF EXISTS course_version_updated_at;
DROP TRIGGER IF EXISTS course_base_has_course_version_updated_at;
DROP TRIGGER IF EXISTS course_version_has_responsible_person_updated_at;
DROP TRIGGER IF EXISTS module_version_has_course_version_updated_at;
DROP TRIGGER IF EXISTS exam_base_updated_at;
DROP TRIGGER IF EXISTS exam_detail_updated_at;
DROP TRIGGER IF EXISTS exam_version_updated_at;
DROP TRIGGER IF EXISTS exam_base_has_exam_version_updated_at;
DROP TRIGGER IF EXISTS exam_version_has_tucan_exam_id_updated_at;
DROP TRIGGER IF EXISTS exam_version_has_responsible_person_updated_at;
DROP TRIGGER IF EXISTS module_version_has_exam_version_updated_at;
DROP TRIGGER IF EXISTS document_category_updated_at;
DROP TRIGGER IF EXISTS document_type_updated_at;
DROP TRIGGER IF EXISTS document_template_updated_at;
DROP TRIGGER IF EXISTS document_template_has_template_updated_at;
DROP TRIGGER IF EXISTS part_key_updated_at;
DROP TRIGGER IF EXISTS part_value_updated_at;
DROP TRIGGER IF EXISTS document_template_has_part_key_updated_at;
DROP TRIGGER IF EXISTS document_template_has_file_format_updated_at;
DROP TRIGGER IF EXISTS setting_key_updated_at;
DROP TRIGGER IF EXISTS setting_value_updated_at;
DROP TRIGGER IF EXISTS document_template_has_setting_key_updated_at;
DROP TRIGGER IF EXISTS feature_updated_at;
DROP TRIGGER IF EXISTS feature_has_setting_key_updated_at;
DROP TRIGGER IF EXISTS data_import_log_updated_at;

-- Drop all tables
DROP TABLE IF EXISTS data_import_log;
DROP TABLE IF EXISTS feature_has_setting_key;
DROP TABLE IF EXISTS feature;
DROP TABLE IF EXISTS document_template_has_setting_key;
DROP TABLE IF EXISTS setting_value;
DROP TABLE IF EXISTS setting_key;
DROP TABLE IF EXISTS document_template_has_file_format;
DROP TABLE IF EXISTS document_template_has_part_key;
DROP TABLE IF EXISTS part_value;
DROP TABLE IF EXISTS part_key;
DROP TABLE IF EXISTS document_template_has_template;
DROP TABLE IF EXISTS document_template;
DROP TABLE IF EXISTS document_type;
DROP TABLE IF EXISTS document_category;
DROP TABLE IF EXISTS module_version_has_exam_version;
DROP TABLE IF EXISTS exam_version_has_responsible_person;
DROP TABLE IF EXISTS exam_version_has_tucan_exam_id;
DROP TABLE IF EXISTS exam_base_has_exam_version;
DROP TABLE IF EXISTS exam_version;
DROP TABLE IF EXISTS exam_detail;
DROP TABLE IF EXISTS exam_base;
DROP TABLE IF EXISTS module_version_has_course_version;
DROP TABLE IF EXISTS course_version_has_responsible_person;
DROP TABLE IF EXISTS course_base_has_course_version;
DROP TABLE IF EXISTS course_version;
DROP TABLE IF EXISTS course_detail;
DROP TABLE IF EXISTS course_base;
DROP TABLE IF EXISTS module_version_has_responsible_person;
DROP TABLE IF EXISTS module_base_has_module_version;
DROP TABLE IF EXISTS module_version;
DROP TABLE IF EXISTS module_detail;
DROP TABLE IF EXISTS module_base;
DROP TABLE IF EXISTS role_has_permission_and_resource;
DROP TABLE IF EXISTS permission_has_resource;
DROP TABLE IF EXISTS account_has_role;
DROP TABLE IF EXISTS role_has_role;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS organization_has_person;
DROP TABLE IF EXISTS organization;
DROP TABLE IF EXISTS account_tuda;
DROP TABLE IF EXISTS account_local;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS person_profile;
DROP TABLE IF EXISTS person_preference;

-- Drop all master data tables
DROP TABLE IF EXISTS external_data;
DROP TABLE IF EXISTS external_system;
DROP TABLE IF EXISTS sex;
DROP TABLE IF EXISTS web_language;
DROP TABLE IF EXISTS responsibility;
DROP TABLE IF EXISTS resource;
DROP TABLE IF EXISTS permission;
DROP TABLE IF EXISTS document_area;
DROP TABLE IF EXISTS weighting_method;
DROP TABLE IF EXISTS grading_system;
DROP TABLE IF EXISTS rotation;
DROP TABLE IF EXISTS duration;
DROP TABLE IF EXISTS title;
DROP TABLE IF EXISTS teaching_language;
DROP TABLE IF EXISTS course_category;
DROP TABLE IF EXISTS course_type;
DROP TABLE IF EXISTS semester;
DROP TABLE IF EXISTS term;
DROP TABLE IF EXISTS exam_type;
DROP TABLE IF EXISTS exam_category;
DROP TABLE IF EXISTS file_format;
DROP TABLE IF EXISTS city;
DROP TABLE IF EXISTS country;