-- noinspection SqlNoDataSourceInspectionForFile

-- DO NOT CHANGE THE ORDER!

USE testvfl;

-----------------------------------------------------------
-- All master data tables
-----------------------------------------------------------

-- Country
INSERT INTO country (numeric_code, alpha_2, alpha_3, name_de, name_en)
VALUES (250, N'fr', N'fra', N'Frankreich', N'France');
INSERT INTO country (numeric_code, alpha_2, alpha_3, name_de, name_en)
VALUES (276, N'de', N'deu', N'Deutschland', N'Germany');
INSERT INTO country (numeric_code, alpha_2, alpha_3, name_de, name_en)
VALUES (392, N'jp', N'jpn', N'Japan', N'Japan');


-- City
INSERT INTO city (subdivision, name_de, name_en, name_de_wo_diacritics, name_en_wo_diacritics,
                  latitude, longitude, country_id)
VALUES (N'BE', N'Berlin', N'Berlin', N'Berlin', N'Berlin', 52.517632, 13.409657, 2);
INSERT INTO city (subdivision, name_de, name_en, name_de_wo_diacritics, name_en_wo_diacritics,
                  latitude, longitude, country_id)
VALUES (N'HE', N'Darmstadt', N'Darmstadt', N'Darmstadt', N'Darmstadt', 49.872582, 8.649083, 2);
INSERT INTO city (subdivision, name_de, name_en, name_de_wo_diacritics, name_en_wo_diacritics,
                  latitude, longitude, country_id)
VALUES (N'PAR', N'Paris', N'Paris', N'Paris', N'Paris', 48.856788, 2.351077, 1);


-- File Format
INSERT INTO file_format (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en)
VALUES (N'PdfAbbrDe', N'PdfAbbrEn', N'PdfNameDe', N'PdfNameEn', N'Transportables Dokumentenformat',
        N'Portable document format');
INSERT INTO file_format (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en)
VALUES (N'LatexAbbrDe', N'LatexAbbrEn', N'LatexNameDe', N'LatexNameEn', N'LaTeX Dokumentenformat',
        N'LaTeX document format');
INSERT INTO file_format (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en)
VALUES (N'DocxAbbrDe', N'DocxAbbrEn', N'DocxNameDe', N'DocxNameEn',
        N'Microsoft Word Dokumentenformat',
        N'Microsoft word document format');


-- Examination category
INSERT INTO exam_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'FP', N'TE', N'Fachprüfung', N'Technical examination',
        N'Fachprüfungen sind Prüfungen, die nur zweimal wiederholt werden dürfen, sodass insgesamt drei Versuche zur Verfügung stehen.',
        N'Technical examinations are examinations that may only be repeated twice, so that a total of three examinations are available.');
INSERT INTO exam_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'SL', N'SA', N'Studienleistung', N'Study achievement',
        N'Studienleistungen sind Prüfungsleistungen, die beliebig oft wiederholt werden können.',
        N'Study achievements are examinations that can be repeated as often as desired.');
INSERT INTO exam_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'AP', N'FE', N'Abschlussprüfung', N'Final examination', N'', N'');
INSERT INTO exam_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'SK', N'DC', N'Standardkategorie', N'Default category',
        N'Diese Kategorie soll nicht mehr verwendet werden.',
        N'This category should no longer be used.');


-- Examination type
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Abgabe', N'Delivery', N'Abgabe', N'Delivery', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Bericht', N'Report', N'Bericht', N'Report', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Essay', N'Essay', N'Essay', N'Essay', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Fachprüfung', N'Techn. exam', N'Fachprüfung', N'Technical examination', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'fakultativ', N'optional', N'fakultativ', N'optional', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Hausarbeit', N'Domestic work', N'Hausarbeit', N'Domestic work', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Hausüb., Arbeitsbl.', N'Homework, worksheet', N'Hausübungen, Arbeitsblätter',
        N'Homework, worksheet', N'', N''); -- TODO: Changed abbreviations to shorter Strings
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Klausur', N'Examination', N'Klausur', N'Examination', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Kolloquium', N'Colloquium', N'Kolloquium', N'Colloquium', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'm/s Pr.', N'Oral/writt. exam.', N'mündliche / schriftliche Prüfung',
        N'Oral/written examination', N'', N''); -- TODO: Changed abbreviations to shorter Strings
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'mündl. Prüfung', N'Oral examination', N'mündliche Prüfung', N'Oral examination', N'',
        N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Portfolio', N'Portfolio', N'Portfolio', N'Portfolio', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Präsentation', N'Presentation', N'Präsentation', N'Presentation', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Protokoll', N'Protocol', N'Protokoll', N'Protocol', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Referat', N'Paper', N'Referat', N'Paper', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'schriftl. Prüfung', N'Written examination', N'schriftliche Prüfung',
        N'Written examination',
        N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Sonderform', N'Special form', N'Sonderform', N'Special form', N'', N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Studienleistung', N'Study achievement', N'Studienleistung', N'Study achievement', N'',
        N'');
INSERT INTO exam_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                       description_en)
VALUES (N'Teiln. Sprachk.', N'Part. lang. cou.', N'Teilnahme Sprachkurs',
        N'Participation language course', N'', N'');


-- Term
INSERT INTO term (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                  description_en)
VALUES (N'WiSe', N'WiSe', N'Wintersemester', N'Winter semester',
        N'Das Wintersemester erstreckt sich vom 1. Oktober bis zum 31. März des Folgejahres.',
        N'The winter semester runs from 1 October to 31 March of the following year.');
INSERT INTO term (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                  description_en)
VALUES (N'SoSe', N'SuSe', N'Sommersemester', N'Summer semester',
        N'Das Sommersemester erstreckt sich vom 1. April bis zum 30. September.',
        N'The sommer semester runs from 1 April to 30 September.');


-- Semester
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 80/81', N'WiSe 80/81', N'Wintersemester 1980/81', N'Winter semester 1980/81',
        N'Das Wintersemester 1980/81 beginnt am 01.10.1980 und endet am 31.03.1981.',
        N'The winter semester 1980/81 starts on 01.10.1980 and ends on 31.03.1981.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 81', N'SuSe 81', N'Sommersemester 1981', N'Summer semester 1981',
        N'Das Sommersemester 1981 beginnt am 01.04.1981 und endet am 30.09.1981.',
        N'The summer semester 1981 starts on 01.04.1981 and ends on 30.09.1981.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 81/82', N'WiSe 81/82', N'Wintersemester 1981/82', N'Winter semester 1981/82',
        N'Das Wintersemester 1981/82 beginnt am 01.10.1981 und endet am 31.03.1982.',
        N'The winter semester 1981/82 starts on 01.10.1981 and ends on 31.03.1982.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 82', N'SuSe 82', N'Sommersemester 1982', N'Summer semester 1982',
        N'Das Sommersemester 1982 beginnt am 01.04.1982 und endet am 30.09.1982.',
        N'The summer semester 1982 starts on 01.04.1982 and ends on 30.09.1982.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 82/83', N'WiSe 82/83', N'Wintersemester 1982/83', N'Winter semester 1982/83',
        N'Das Wintersemester 1982/83 beginnt am 01.10.1982 und endet am 31.03.1983.',
        N'The winter semester 1982/83 starts on 01.10.1982 and ends on 31.03.1983.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 83', N'SuSe 83', N'Sommersemester 1983', N'Summer semester 1983',
        N'Das Sommersemester 1983 beginnt am 01.04.1983 und endet am 30.09.1983.',
        N'The summer semester 1983 starts on 01.04.1983 and ends on 30.09.1983.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 83/84', N'WiSe 83/84', N'Wintersemester 1983/84', N'Winter semester 1983/84',
        N'Das Wintersemester 1983/84 beginnt am 01.10.1983 und endet am 31.03.1984.',
        N'The winter semester 1983/84 starts on 01.10.1983 and ends on 31.03.1984.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 84', N'SuSe 84', N'Sommersemester 1984', N'Summer semester 1984',
        N'Das Sommersemester 1984 beginnt am 01.04.1984 und endet am 30.09.1984.',
        N'The summer semester 1984 starts on 01.04.1984 and ends on 30.09.1984.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 84/85', N'WiSe 84/85', N'Wintersemester 1984/85', N'Winter semester 1984/85',
        N'Das Wintersemester 1984/85 beginnt am 01.10.1984 und endet am 31.03.1985.',
        N'The winter semester 1984/85 starts on 01.10.1984 and ends on 31.03.1985.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 85', N'SuSe 85', N'Sommersemester 1985', N'Summer semester 1985',
        N'Das Sommersemester 1985 beginnt am 01.04.1985 und endet am 30.09.1985.',
        N'The summer semester 1985 starts on 01.04.1985 and ends on 30.09.1985.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 85/86', N'WiSe 85/86', N'Wintersemester 1985/86', N'Winter semester 1985/86',
        N'Das Wintersemester 1985/86 beginnt am 01.10.1985 und endet am 31.03.1986.',
        N'The winter semester 1985/86 starts on 01.10.1985 and ends on 31.03.1986.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 86', N'SuSe 86', N'Sommersemester 1986', N'Summer semester 1986',
        N'Das Sommersemester 1986 beginnt am 01.04.1986 und endet am 30.09.1986.',
        N'The summer semester 1986 starts on 01.04.1986 and ends on 30.09.1986.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 86/87', N'WiSe 86/87', N'Wintersemester 1986/87', N'Winter semester 1986/87',
        N'Das Wintersemester 1986/87 beginnt am 01.10.1986 und endet am 31.03.1987.',
        N'The winter semester 1986/87 starts on 01.10.1986 and ends on 31.03.1987.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 87', N'SuSe 87', N'Sommersemester 1987', N'Summer semester 1987',
        N'Das Sommersemester 1987 beginnt am 01.04.1987 und endet am 30.09.1984.',
        N'The summer semester 1987 starts on 01.04.1987 and ends on 30.09.1987.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 87/88', N'WiSe 87/88', N'Wintersemester 1987/88', N'Winter semester 1987/88',
        N'Das Wintersemester 1987/88 beginnt am 01.10.1987 und endet am 31.03.1988.',
        N'The winter semester 1987/88 starts on 01.10.1987 and ends on 31.03.1988.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 88', N'SuSe 88', N'Sommersemester 1988', N'Summer semester 1988',
        N'Das Sommersemester 1988 beginnt am 01.04.1988 und endet am 30.09.1988.',
        N'The summer semester 1988 starts on 01.04.1988 and ends on 30.09.1988.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 88/89', N'WiSe 88/89', N'Wintersemester 1988/89', N'Winter semester 1988/89',
        N'Das Wintersemester 1988/89 beginnt am 01.10.1988 und endet am 31.03.1989.',
        N'The winter semester 1988/89 starts on 01.10.1988 and ends on 31.03.1989.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 89', N'SuSe 89', N'Sommersemester 1989', N'Summer semester 1989',
        N'Das Sommersemester 1989 beginnt am 01.04.1989 und endet am 30.09.1989.',
        N'The summer semester 1989 starts on 01.04.1989 and ends on 30.09.1989.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 89/90', N'WiSe 89/90', N'Wintersemester 1989/90', N'Winter semester 1989/90',
        N'Das Wintersemester 1989/90 beginnt am 01.10.1989 und endet am 31.03.1990.',
        N'The winter semester 1989/90 starts on 01.10.1989 and ends on 31.03.1990.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 90', N'SuSe 90', N'Sommersemester 1990', N'Summer semester 1990',
        N'Das Sommersemester 1990 beginnt am 01.04.1990 und endet am 30.09.1990.',
        N'The summer semester 1990 starts on 01.04.1990 and ends on 30.09.1990.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 90/91', N'WiSe 90/91', N'Wintersemester 1990/91', N'Winter semester 1990/91',
        N'Das Wintersemester 1990/91 beginnt am 01.10.1990 und endet am 31.03.1991.',
        N'The winter semester 1990/91 starts on 01.10.1990 and ends on 31.03.1991.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 91', N'SuSe 91', N'Sommersemester 1991', N'Summer semester 1991',
        N'Das Sommersemester 1991 beginnt am 01.04.1991 und endet am 30.09.1991.',
        N'The summer semester 1991 starts on 01.04.1991 and ends on 30.09.1991.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 91/92', N'WiSe 91/92', N'Wintersemester 1991/92', N'Winter semester 1991/92',
        N'Das Wintersemester 1991/92 beginnt am 01.10.1991 und endet am 31.03.1992.',
        N'The winter semester 1991/92 starts on 01.10.1991 and ends on 31.03.1992.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 92', N'SuSe 92', N'Sommersemester 1992', N'Summer semester 1992',
        N'Das Sommersemester 1992 beginnt am 01.04.1992 und endet am 30.09.1992.',
        N'The summer semester 1992 starts on 01.04.1992 and ends on 30.09.1992.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 92/93', N'WiSe 92/93', N'Wintersemester 1992/93', N'Winter semester 1992/93',
        N'Das Wintersemester 1992/93 beginnt am 01.10.1992 und endet am 31.03.1993.',
        N'The winter semester 1992/93 starts on 01.10.1992 and ends on 31.03.1993.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 93', N'SuSe 93', N'Sommersemester 1993', N'Summer semester 1993',
        N'Das Sommersemester 1993 beginnt am 01.04.1993 und endet am 30.09.1993.',
        N'The summer semester 1993 starts on 01.04.1993 and ends on 30.09.1993.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 93/94', N'WiSe 93/94', N'Wintersemester 1993/94', N'Winter semester 1993/94',
        N'Das Wintersemester 1993/94 beginnt am 01.10.1993 und endet am 31.03.1994.',
        N'The winter semester 1993/94 starts on 01.10.1994 and ends on 31.03.1994.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 94', N'SuSe 94', N'Sommersemester 1994', N'Summer semester 1994',
        N'Das Sommersemester 1994 beginnt am 01.04.1994 und endet am 30.09.1994.',
        N'The summer semester 1994 starts on 01.04.1994 and ends on 30.09.1994.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 94/95', N'WiSe 94/95', N'Wintersemester 1994/95', N'Winter semester 1994/95',
        N'Das Wintersemester 1994/95 beginnt am 01.10.1994 und endet am 31.03.1995.',
        N'The winter semester 1994/95 starts on 01.10.1994 and ends on 31.03.1995.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 95', N'SuSe 95', N'Sommersemester 1995', N'Summer semester 1995',
        N'Das Sommersemester 1995 beginnt am 01.04.1995 und endet am 30.09.1995.',
        N'The summer semester 1995 starts on 01.04.1995 and ends on 30.09.1995.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 95/96', N'WiSe 95/96', N'Wintersemester 1995/96', N'Winter semester 1995/96',
        N'Das Wintersemester 1995/96 beginnt am 01.10.1995 und endet am 31.03.1996.',
        N'The winter semester 1995/96 starts on 01.10.1995 and ends on 31.03.1996.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 96', N'SuSe 96', N'Sommersemester 1996', N'Summer semester 1996',
        N'Das Sommersemester 1996 beginnt am 01.04.1996 und endet am 30.09.1996.',
        N'The summer semester 1996 starts on 01.04.1996 and ends on 30.09.1996.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 96/97', N'WiSe 96/97', N'Wintersemester 1996/97', N'Winter semester 1996/97',
        N'Das Wintersemester 1996/97 beginnt am 01.10.1996 und endet am 31.03.1997.',
        N'The winter semester 1996/97 starts on 01.10.1996 and ends on 31.03.1997.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 97', N'SuSe 97', N'Sommersemester 1997', N'Summer semester 1997',
        N'Das Sommersemester 1997 beginnt am 01.04.1997 und endet am 30.09.1997.',
        N'The summer semester 1997 starts on 01.04.1997 and ends on 30.09.1997.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 97/98', N'WiSe 97/98', N'Wintersemester 1997/98', N'Winter semester 1997/98',
        N'Das Wintersemester 1997/98 beginnt am 01.10.1997 und endet am 31.03.1998.',
        N'The winter semester 1997/98 starts on 01.10.1997 and ends on 31.03.1998.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 98', N'SuSe 98', N'Sommersemester 1998', N'Summer semester 1998',
        N'Das Sommersemester 1998 beginnt am 01.04.1998 und endet am 30.09.1998.',
        N'The summer semester 1998 starts on 01.04.1998 and ends on 30.09.1998.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 98/99', N'WiSe 98/99', N'Wintersemester 1998/99', N'Winter semester 1998/99',
        N'Das Wintersemester 1998/99 beginnt am 01.10.1998 und endet am 31.03.1999.',
        N'The winter semester 1998/99 starts on 01.10.1998 and ends on 31.03.1999.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 99', N'SuSe 99', N'Sommersemester 1999', N'Summer semester 1999',
        N'Das Sommersemester 1999 beginnt am 01.04.1999 und endet am 30.09.1999.',
        N'The summer semester 1999 starts on 01.04.1999 and ends on 30.09.1999.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 99/00', N'WiSe 99/00', N'Wintersemester 1999/00', N'Winter semester 1999/00',
        N'Das Wintersemester 1999/00 beginnt am 01.10.1999 und endet am 31.03.2000.',
        N'The winter semester 1999/00 starts on 01.10.1999 and ends on 31.03.2000.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 00', N'SuSe 00', N'Sommersemester 2000', N'Summer semester 2000',
        N'Das Sommersemester 2000 beginnt am 01.04.2000 und endet am 30.09.2000.',
        N'The summer semester 2000 starts on 01.04.2000 and ends on 30.09.2000.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 00/01', N'WiSe 00/01', N'Wintersemester 2000/01', N'Winter semester 2000/01',
        N'Das Wintersemester 2000/01 beginnt am 01.10.2000 und endet am 31.03.2001.',
        N'The winter semester 2000/01 starts on 01.10.2000 and ends on 31.03.2001.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 01', N'SuSe 01', N'Sommersemester 2001', N'Summer semester 2001',
        N'Das Sommersemester 2001 beginnt am 01.04.2001 und endet am 30.09.2001.',
        N'The summer semester 2001 starts on 01.04.2001 and ends on 30.09.2001.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 01/02', N'WiSe 01/02', N'Wintersemester 2001/02', N'Winter semester 2001/02',
        N'Das Wintersemester 2001/02 beginnt am 01.10.2001 und endet am 31.03.2002.',
        N'The winter semester 2001/02 starts on 01.10.2001 and ends on 31.03.2002.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 02', N'SuSe 02', N'Sommersemester 2002', N'Summer semester 2002',
        N'Das Sommersemester 2002 beginnt am 01.04.2002 und endet am 30.09.2002.',
        N'The summer semester 2002 starts on 01.04.2002 and ends on 30.09.2002.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 02/03', N'WiSe 02/03', N'Wintersemester 2002/03', N'Winter semester 2002/03',
        N'Das Wintersemester 2002/03 beginnt am 01.10.2002 und endet am 31.03.2003.',
        N'The winter semester 2002/03 starts on 01.10.2002 and ends on 31.03.2003.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 03', N'SuSe 03', N'Sommersemester 2003', N'Summer semester 2003',
        N'Das Sommersemester 2003 beginnt am 01.04.2003 und endet am 30.09.2003.',
        N'The summer semester 2003 starts on 01.04.2003 and ends on 30.09.2003.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 03/04', N'WiSe 03/04', N'Wintersemester 2003/04', N'Winter semester 2003/04',
        N'Das Wintersemester 2003/04 beginnt am 01.10.2003 und endet am 31.03.2004.',
        N'The winter semester 2003/04 starts on 01.10.2003 and ends on 31.03.2004.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 04', N'SuSe 04', N'Sommersemester 2004', N'Summer semester 2004',
        N'Das Sommersemester 2004 beginnt am 01.04.2004 und endet am 30.09.2004.',
        N'The summer semester 2004 starts on 01.04.2001 and ends on 30.09.2004.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 04/05', N'WiSe 04/05', N'Wintersemester 2004/05', N'Winter semester 2004/05',
        N'Das Wintersemester 2004/05 beginnt am 01.10.2004 und endet am 31.03.2005.',
        N'The winter semester 2004/05 starts on 01.10.2004 and ends on 31.03.2005.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 05', N'SuSe 05', N'Sommersemester 2005', N'Summer semester 2005',
        N'Das Sommersemester 2005 beginnt am 01.04.2005 und endet am 30.09.2005.',
        N'The summer semester 2005 starts on 01.04.2005 and ends on 30.09.2005.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 05/06', N'WiSe 05/06', N'Wintersemester 2005/06', N'Winter semester 2005/06',
        N'Das Wintersemester 2005/06 beginnt am 01.10.2005 und endet am 31.03.2006.',
        N'The winter semester 2005/06 starts on 01.10.2005 and ends on 31.03.2006.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 06', N'SuSe 06', N'Sommersemester 2006', N'Summer semester 2006',
        N'Das Sommersemester 2006 beginnt am 01.04.2006 und endet am 30.09.2006.',
        N'The summer semester 2006 starts on 01.04.2006 and ends on 30.09.2006.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 06/07', N'WiSe 06/07', N'Wintersemester 2006/07', N'Winter semester 2006/07',
        N'Das Wintersemester 2006/07 beginnt am 01.10.2006 und endet am 31.03.2007.',
        N'The winter semester 2006/07 starts on 01.10.2006 and ends on 31.03.2007.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 07', N'SuSe 07', N'Sommersemester 2007', N'Summer semester 2007',
        N'Das Sommersemester 2007 beginnt am 01.04.2007 und endet am 30.09.2007.',
        N'The summer semester 2007 starts on 01.04.2007 and ends on 30.09.2007.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 07/08', N'WiSe 07/08', N'Wintersemester 2007/08', N'Winter semester 2007/08',
        N'Das Wintersemester 2007/08 beginnt am 01.10.2007 und endet am 31.03.2008.',
        N'The winter semester 2007/08 starts on 01.10.2007 and ends on 31.03.2008.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 08', N'SuSe 08', N'Sommersemester 2008', N'Summer semester 2008',
        N'Das Sommersemester 2008 beginnt am 01.04.2008 und endet am 30.09.2008.',
        N'The summer semester 2008 starts on 01.04.2008 and ends on 30.09.2008.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 08/09', N'WiSe 08/09', N'Wintersemester 2008/09', N'Winter semester 2008/09',
        N'Das Wintersemester 2008/09 beginnt am 01.10.2008 und endet am 31.03.2009.',
        N'The winter semester 2008/09 starts on 01.10.2008 and ends on 31.03.2009.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 09', N'SuSe 09', N'Sommersemester 2009', N'Summer semester 2009',
        N'Das Sommersemester 2009 beginnt am 01.04.2009 und endet am 30.09.2009.',
        N'The summer semester 2009 starts on 01.04.2009 and ends on 30.09.2009.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 09/10', N'WiSe 09/10', N'Wintersemester 2009/10', N'Winter semester 2009/10',
        N'Das Wintersemester 2009/10 beginnt am 01.10.2009 und endet am 31.03.2010.',
        N'The winter semester 2009/10 starts on 01.10.2009 and ends on 31.03.2010.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 10', N'SuSe 10', N'Sommersemester 2010', N'Summer semester 2010',
        N'Das Sommersemester 2010 beginnt am 01.04.2010 und endet am 30.09.2010.',
        N'The summer semester 2010 starts on 01.04.2010 and ends on 30.09.2010.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 10/11', N'WiSe 10/11', N'Wintersemester 2010/11', N'Winter semester 2010/11',
        N'Das Wintersemester 2010/11 beginnt am 01.10.2010 und endet am 31.03.2011.',
        N'The winter semester 2010/11 starts on 01.10.2010 and ends on 31.03.2011.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 11', N'SuSe 11', N'Sommersemester 2011', N'Summer semester 2011',
        N'Das Sommersemester 2011 beginnt am 01.04.2011 und endet am 30.09.2011.',
        N'The summer semester 2011 starts on 01.04.2011 and ends on 30.09.2011.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 11/12', N'WiSe 11/12', N'Wintersemester 2011/12', N'Winter semester 2011/12',
        N'Das Wintersemester 2011/12 beginnt am 01.10.2011 und endet am 31.03.2012.',
        N'The winter semester 2011/12 starts on 01.10.2011 and ends on 31.03.2012.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 12', N'SuSe 12', N'Sommersemester 2012', N'Summer semester 2012',
        N'Das Sommersemester 2012 beginnt am 01.04.2012 und endet am 30.09.2012.',
        N'The summer semester 2012 starts on 01.04.2012 and ends on 30.09.2012.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 12/13', N'WiSe 12/13', N'Wintersemester 2012/13', N'Winter semester 2012/13',
        N'Das Wintersemester 2012/13 beginnt am 01.10.2012 und endet am 31.03.2013.',
        N'The winter semester 2012/13 starts on 01.10.2012 and ends on 31.03.2013.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 13', N'SuSe 13', N'Sommersemester 2013', N'Summer semester 2013',
        N'Das Sommersemester 2013 beginnt am 01.04.2013 und endet am 30.09.2013.',
        N'The summer semester 2013 starts on 01.04.2013 and ends on 30.09.2013.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 13/14', N'WiSe 13/14', N'Wintersemester 2013/14', N'Winter semester 2013/14',
        N'Das Wintersemester 2013/14 beginnt am 01.10.2013 und endet am 31.03.2014.',
        N'The winter semester 2013/14 starts on 01.10.2013 and ends on 31.03.2014.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 14', N'SuSe 14', N'Sommersemester 2014', N'Summer semester 2014',
        N'Das Sommersemester 2014 beginnt am 01.04.2014 und endet am 30.09.2014.',
        N'The summer semester 2014 starts on 01.04.2014 and ends on 30.09.2014.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 14/15', N'WiSe 14/15', N'Wintersemester 2014/15', N'Winter semester 2014/15',
        N'Das Wintersemester 2014/15 beginnt am 01.10.2014 und endet am 31.03.2015.',
        N'The winter semester 2014/15 starts on 01.10.2014 and ends on 31.03.2015.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 15', N'SuSe 15', N'Sommersemester 2015', N'Summer semester 2015',
        N'Das Sommersemester 2015 beginnt am 01.04.2015 und endet am 30.09.2015.',
        N'The summer semester 2015 starts on 01.04.2015 and ends on 30.09.2015.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 15/16', N'WiSe 15/16', N'Wintersemester 2015/16', N'Winter semester 2015/16',
        N'Das Wintersemester 2015/16 beginnt am 01.10.2015 und endet am 31.03.2016.',
        N'The winter semester 2015/16 starts on 01.10.2015 and ends on 31.03.2016.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 16', N'SuSe 16', N'Sommersemester 2016', N'Summer semester 2016',
        N'Das Sommersemester 2016 beginnt am 01.04.2016 und endet am 30.09.2016.',
        N'The summer semester 2016 starts on 01.04.2016 and ends on 30.09.2016.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 16/17', N'WiSe 16/17', N'Wintersemester 2016/17', N'Winter semester 2016/17',
        N'Das Wintersemester 2016/17 beginnt am 01.10.2016 und endet am 31.03.2017.',
        N'The winter semester 2016/17 starts on 01.10.2016 and ends on 31.03.2017.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 17', N'SuSe 17', N'Sommersemester 2017', N'Summer semester 2017',
        N'Das Sommersemester 2017 beginnt am 01.04.2017 und endet am 30.09.2017.',
        N'The summer semester 2017 starts on 01.04.2017 and ends on 30.09.2017.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 17/18', N'WiSe 17/18', N'Wintersemester 2017/18', N'Winter semester 2017/18',
        N'Das Wintersemester 2017/18 beginnt am 01.10.2017 und endet am 31.03.2018.',
        N'The winter semester 2017/18 starts on 01.10.2017 and ends on 31.03.2018.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 18', N'SuSe 18', N'Sommersemester 2018', N'Summer semester 2018',
        N'Das Sommersemester 2018 beginnt am 01.04.2018 und endet am 30.09.2018.',
        N'The summer semester 2018 starts on 01.04.2018 and ends on 30.09.2018.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 18/19', N'WiSe 18/19', N'Wintersemester 2018/19', N'Winter semester 2018/19',
        N'Das Wintersemester 2018/19 beginnt am 01.10.2018 und endet am 31.03.2019.',
        N'The winter semester 2018/19 starts on 01.10.2018 and ends on 31.03.2019.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 19', N'SuSe 19', N'Sommersemester 2019', N'Summer semester 2019',
        N'Das Sommersemester 2019 beginnt am 01.04.2019 und endet am 30.09.2019.',
        N'The summer semester 2019 starts on 01.04.2019 and ends on 30.09.2019.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 19/20', N'WiSe 19/20', N'Wintersemester 2019/20', N'Winter semester 2019/20',
        N'Das Wintersemester 2019/20 beginnt am 01.10.2019 und endet am 31.03.2020.',
        N'The winter semester 2019/20 starts on 01.10.2019 and ends on 31.03.2020.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 20', N'SuSe 20', N'Sommersemester 2020', N'Summer semester 2020',
        N'Das Sommersemester 2020 beginnt am 01.04.2020 und endet am 30.09.2020.',
        N'The summer semester 2020 starts on 01.04.2020 and ends on 30.09.2020.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 20/21', N'WiSe 20/21', N'Wintersemester 2020/21', N'Winter semester 2020/21',
        N'Das Wintersemester 2020/21 beginnt am 01.10.2020 und endet am 31.03.2021.',
        N'The winter semester 2020/21 starts on 01.10.2020 and ends on 31.03.2021.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 21', N'SuSe 21', N'Sommersemester 2021', N'Summer semester 2021',
        N'Das Sommersemester 2021 beginnt am 01.04.2021 und endet am 30.09.2021.',
        N'The summer semester 2021 starts on 01.04.2021 and ends on 30.09.2021.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 21/22', N'WiSe 21/22', N'Wintersemester 2021/22', N'Winter semester 2021/22',
        N'Das Wintersemester 2021/22 beginnt am 01.10.2021 und endet am 31.03.2022.',
        N'The winter semester 2021/22 starts on 01.10.2021 and ends on 31.03.2022.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 22', N'SuSe 22', N'Sommersemester 2022', N'Summer semester 2022',
        N'Das Sommersemester 2022 beginnt am 01.04.2022 und endet am 30.09.2022.',
        N'The summer semester 2022 starts on 01.04.2022 and ends on 30.09.2022.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 22/23', N'WiSe 22/23', N'Wintersemester 2022/23', N'Winter semester 2022/23',
        N'Das Wintersemester 2022/23 beginnt am 01.10.2022 und endet am 31.03.2023.',
        N'The winter semester 2022/23 starts on 01.10.2022 and ends on 31.03.2023.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 23', N'SuSe 23', N'Sommersemester 2023', N'Summer semester 2023',
        N'Das Sommersemester 2023 beginnt am 01.04.2023 und endet am 30.09.2023.',
        N'The summer semester 2023 starts on 01.04.2023 and ends on 30.09.2023.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 23/24', N'WiSe 23/24', N'Wintersemester 2023/24', N'Winter semester 2023/24',
        N'Das Wintersemester 2023/24 beginnt am 01.10.2023 und endet am 31.03.2024.',
        N'The winter semester 2023/24 starts on 01.10.2023 and ends on 31.03.2024.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 24', N'SuSe 24', N'Sommersemester 2024', N'Summer semester 2024',
        N'Das Sommersemester 2024 beginnt am 01.04.2024 und endet am 30.09.2024.',
        N'The summer semester 2024 starts on 01.04.2024 and ends on 30.09.2024.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 24/25', N'WiSe 24/25', N'Wintersemester 2024/25', N'Winter semester 2024/25',
        N'Das Wintersemester 2024/25 beginnt am 01.10.2024 und endet am 31.03.2025.',
        N'The winter semester 2024/25 starts on 01.10.2024 and ends on 31.03.2025.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 25', N'SuSe 25', N'Sommersemester 2025', N'Summer semester 2025',
        N'Das Sommersemester 2025 beginnt am 01.04.2025 und endet am 30.09.2025.',
        N'The summer semester 2025 starts on 01.04.2025 and ends on 30.09.2025.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 25/26', N'WiSe 25/26', N'Wintersemester 2025/26', N'Winter semester 2025/26',
        N'Das Wintersemester 2025/26 beginnt am 01.10.2025 und endet am 31.03.2026.',
        N'The winter semester 2025/26 starts on 01.10.2025 and ends on 31.03.2026.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 26', N'SuSe 26', N'Sommersemester 2026', N'Summer semester 2026',
        N'Das Sommersemester 2026 beginnt am 01.04.2026 und endet am 30.09.2026.',
        N'The summer semester 2026 starts on 01.04.2026 and ends on 30.09.2026.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 26/27', N'WiSe 26/27', N'Wintersemester 2026/27', N'Winter semester 2026/27',
        N'Das Wintersemester 2026/27 beginnt am 01.10.2026 und endet am 31.03.2027.',
        N'The winter semester 2026/27 starts on 01.10.2026 and ends on 31.03.2027.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 27', N'SuSe 27', N'Sommersemester 2027', N'Summer semester 2027',
        N'Das Sommersemester 2027 beginnt am 01.04.2027 und endet am 30.09.2027.',
        N'The summer semester 2027 starts on 01.04.2027 and ends on 30.09.2027.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 27/28', N'WiSe 27/28', N'Wintersemester 2027/28', N'Winter semester 2027/28',
        N'Das Wintersemester 2027/28 beginnt am 01.10.2027 und endet am 31.03.2028.',
        N'The winter semester 2027/28 starts on 01.10.2027 and ends on 31.03.2028.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 28', N'SuSe 28', N'Sommersemester 2028', N'Summer semester 2028',
        N'Das Sommersemester 2028 beginnt am 01.04.2028 und endet am 30.09.2028.',
        N'The summer semester 2028 starts on 01.04.2028 and ends on 30.09.2028.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 28/29', N'WiSe 28/29', N'Wintersemester 2028/29', N'Winter semester 2028/29',
        N'Das Wintersemester 2028/29 beginnt am 01.10.2028 und endet am 31.03.2029.',
        N'The winter semester 2028/29 starts on 01.10.2028 and ends on 31.03.2029.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 29', N'SuSe 29', N'Sommersemester 2029', N'Summer semester 2029',
        N'Das Sommersemester 2029 beginnt am 01.04.2029 und endet am 30.09.2029.',
        N'The summer semester 2029 starts on 01.04.2029 and ends on 30.09.2029.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 29/30', N'WiSe 29/30', N'Wintersemester 2029/30', N'Winter semester 2029/30',
        N'Das Wintersemester 2029/30 beginnt am 01.10.2029 und endet am 31.03.2030.',
        N'The winter semester 2029/30 starts on 01.10.2029 and ends on 31.03.2030.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 30', N'SuSe 30', N'Sommersemester 2030', N'Summer semester 2030',
        N'Das Sommersemester 2030 beginnt am 01.04.2030 und endet am 30.09.2030.',
        N'The summer semester 2030 starts on 01.04.2030 and ends on 30.09.2030.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 30/31', N'WiSe 30/31', N'Wintersemester 2030/31', N'Winter semester 2030/31',
        N'Das Wintersemester 2030/31 beginnt am 01.10.2030 und endet am 31.03.2031.',
        N'The winter semester 2030/31 starts on 01.10.2030 and ends on 31.03.2031.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 31', N'SuSe 31', N'Sommersemester 2031', N'Summer semester 2031',
        N'Das Sommersemester 2031 beginnt am 01.04.2031 und endet am 30.09.2031.',
        N'The summer semester 2031 starts on 01.04.2031 and ends on 30.09.2031.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 31/32', N'WiSe 31/32', N'Wintersemester 2031/32', N'Winter semester 2031/32',
        N'Das Wintersemester 2031/32 beginnt am 01.10.2031 und endet am 31.03.2032.',
        N'The winter semester 2031/32 starts on 01.10.2031 and ends on 31.03.2032.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 32', N'SuSe 32', N'Sommersemester 2032', N'Summer semester 2032',
        N'Das Sommersemester 2032 beginnt am 01.04.2032 und endet am 30.09.2032.',
        N'The summer semester 2032 starts on 01.04.2032 and ends on 30.09.2032.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 32/33', N'WiSe 32/33', N'Wintersemester 2032/33', N'Winter semester 2032/33',
        N'Das Wintersemester 2032/33 beginnt am 01.10.2032 und endet am 31.03.2033.',
        N'The winter semester 2032/33 starts on 01.10.2032 and ends on 31.03.2033.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 33', N'SuSe 33', N'Sommersemester 2033', N'Summer semester 2033',
        N'Das Sommersemester 2033 beginnt am 01.04.2033 und endet am 30.09.2033.',
        N'The summer semester 2033 starts on 01.04.2033 and ends on 30.09.2033.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 33/34', N'WiSe 33/34', N'Wintersemester 2033/34', N'Winter semester 2033/34',
        N'Das Wintersemester 2033/34 beginnt am 01.10.2033 und endet am 31.03.2034.',
        N'The winter semester 2033/34 starts on 01.10.2033 and ends on 31.03.2034.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 34', N'SuSe 34', N'Sommersemester 2034', N'Summer semester 2034',
        N'Das Sommersemester 2034 beginnt am 01.04.2034 und endet am 30.09.2034.',
        N'The summer semester 2034 starts on 01.04.2034 and ends on 30.09.2034.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 34/35', N'WiSe 34/35', N'Wintersemester 2034/35', N'Winter semester 2034/35',
        N'Das Wintersemester 2034/35 beginnt am 01.10.2034 und endet am 31.03.2035.',
        N'The winter semester 2034/35 starts on 01.10.2034 and ends on 31.03.2035.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 35', N'SuSe 35', N'Sommersemester 2035', N'Summer semester 2035',
        N'Das Sommersemester 2035 beginnt am 01.04.2035 und endet am 30.09.2035.',
        N'The summer semester 2035 starts on 01.04.2035 and ends on 30.09.2035.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 35/36', N'WiSe 35/36', N'Wintersemester 2035/36', N'Winter semester 2035/36',
        N'Das Wintersemester 2035/36 beginnt am 01.10.2035 und endet am 31.03.2036.',
        N'The winter semester 2035/36 starts on 01.10.2035 and ends on 31.03.2036.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 36', N'SuSe 36', N'Sommersemester 2036', N'Summer semester 2036',
        N'Das Sommersemester 2036 beginnt am 01.04.2036 und endet am 30.09.2036.',
        N'The summer semester 2036 starts on 01.04.2036 and ends on 30.09.2036.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 36/37', N'WiSe 36/37', N'Wintersemester 2036/37', N'Winter semester 2036/37',
        N'Das Wintersemester 2036/37 beginnt am 01.10.2036 und endet am 31.03.2037.',
        N'The winter semester 2036/37 starts on 01.10.2036 and ends on 31.03.2037.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 37', N'SuSe 37', N'Sommersemester 2037', N'Summer semester 2037',
        N'Das Sommersemester 2037 beginnt am 01.04.2037 und endet am 30.09.2037.',
        N'The summer semester 2037 starts on 01.04.2037 and ends on 30.09.2037.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 37/38', N'WiSe 37/38', N'Wintersemester 2037/38', N'Winter semester 2037/38',
        N'Das Wintersemester 2037/38 beginnt am 01.10.2037 und endet am 31.03.2038.',
        N'The winter semester 2037/38 starts on 01.10.2037 and ends on 31.03.2038.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 38', N'SuSe 38', N'Sommersemester 2038', N'Summer semester 2038',
        N'Das Sommersemester 2038 beginnt am 01.04.2038 und endet am 30.09.2038.',
        N'The summer semester 2038 starts on 01.04.2038 and ends on 30.09.2038.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 38/39', N'WiSe 38/39', N'Wintersemester 2038/39', N'Winter semester 2038/39',
        N'Das Wintersemester 2038/39 beginnt am 01.10.2038 und endet am 31.03.2039.',
        N'The winter semester 2038/39 starts on 01.10.2038 and ends on 31.03.2039.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 39', N'SuSe 39', N'Sommersemester 2039', N'Summer semester 2039',
        N'Das Sommersemester 2039 beginnt am 01.04.2039 und endet am 30.09.2039.',
        N'The summer semester 2039 starts on 01.04.2039 and ends on 30.09.2039.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 39/40', N'WiSe 39/40', N'Wintersemester 2039/40', N'Winter semester 2039/40',
        N'Das Wintersemester 2039/40 beginnt am 01.10.2039 und endet am 31.03.2040.',
        N'The winter semester 2039/40 starts on 01.10.2039 and ends on 31.03.2040.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 40', N'SuSe 40', N'Sommersemester 2040', N'Summer semester 2040',
        N'Das Sommersemester 2040 beginnt am 01.04.2040 und endet am 30.09.2040.',
        N'The summer semester 2040 starts on 01.04.2040 and ends on 30.09.2040.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 40/41', N'WiSe 40/41', N'Wintersemester 2040/41', N'Winter semester 2040/41',
        N'Das Wintersemester 2040/41 beginnt am 01.10.2040 und endet am 31.03.2041.',
        N'The winter semester 2040/41 starts on 01.10.2040 and ends on 31.03.2041.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 41', N'SuSe 41', N'Sommersemester 2041', N'Summer semester 2041',
        N'Das Sommersemester 2041 beginnt am 01.04.2041 und endet am 30.09.2041.',
        N'The summer semester 2041 starts on 01.04.2041 and ends on 30.09.2041.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 41/42', N'WiSe 41/42', N'Wintersemester 2041/42', N'Winter semester 2041/42',
        N'Das Wintersemester 2041/42 beginnt am 01.10.2041 und endet am 31.03.2042.',
        N'The winter semester 2041/42 starts on 01.10.2041 and ends on 31.03.2042.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 42', N'SuSe 42', N'Sommersemester 2042', N'Summer semester 2042',
        N'Das Sommersemester 2042 beginnt am 01.04.2042 und endet am 30.09.2042.',
        N'The summer semester 2042 starts on 01.04.2042 and ends on 30.09.2042.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 42/43', N'WiSe 42/43', N'Wintersemester 2042/43', N'Winter semester 2042/43',
        N'Das Wintersemester 2042/43 beginnt am 01.10.2042 und endet am 31.03.2043.',
        N'The winter semester 2042/43 starts on 01.10.2042 and ends on 31.03.2043.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 43', N'SuSe 43', N'Sommersemester 2043', N'Summer semester 2043',
        N'Das Sommersemester 2043 beginnt am 01.04.2043 und endet am 30.09.2043.',
        N'The summer semester 2043 starts on 01.04.2043 and ends on 30.09.2043.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 43/44', N'WiSe 43/44', N'Wintersemester 2043/44', N'Winter semester 2043/44',
        N'Das Wintersemester 2043/44 beginnt am 01.10.2043 und endet am 31.03.2044.',
        N'The winter semester 2043/44 starts on 01.10.2043 and ends on 31.03.2044.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 44', N'SuSe 44', N'Sommersemester 2044', N'Summer semester 2044',
        N'Das Sommersemester 2044 beginnt am 01.04.2044 und endet am 30.09.2044.',
        N'The summer semester 2044 starts on 01.04.2044 and ends on 30.09.2044.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 44/45', N'WiSe 44/45', N'Wintersemester 2044/45', N'Winter semester 2044/45',
        N'Das Wintersemester 2044/45 beginnt am 01.10.2044 und endet am 31.03.2045.',
        N'The winter semester 2044/45 starts on 01.10.2044 and ends on 31.03.2045.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 45', N'SuSe 45', N'Sommersemester 2045', N'Summer semester 2045',
        N'Das Sommersemester 2045 beginnt am 01.04.2045 und endet am 30.09.2045.',
        N'The summer semester 2045 starts on 01.04.2045 and ends on 30.09.2045.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 45/46', N'WiSe 45/46', N'Wintersemester 2045/46', N'Winter semester 2045/46',
        N'Das Wintersemester 2045/46 beginnt am 01.10.2045 und endet am 31.03.2046.',
        N'The winter semester 2045/46 starts on 01.10.2045 and ends on 31.03.2046.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 46', N'SuSe 46', N'Sommersemester 2046', N'Summer semester 2046',
        N'Das Sommersemester 2046 beginnt am 01.04.2046 und endet am 30.09.2046.',
        N'The summer semester 2046 starts on 01.04.2046 and ends on 30.09.2046.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 46/47', N'WiSe 46/47', N'Wintersemester 2046/47', N'Winter semester 2046/47',
        N'Das Wintersemester 2046/47 beginnt am 01.10.2046 und endet am 31.03.2047.',
        N'The winter semester 2046/47 starts on 01.10.2046 and ends on 31.03.2047.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 47', N'SuSe 47', N'Sommersemester 2047', N'Summer semester 2047',
        N'Das Sommersemester 2047 beginnt am 01.04.2047 und endet am 30.09.2047.',
        N'The summer semester 2047 starts on 01.04.2047 and ends on 30.09.2047.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 47/48', N'WiSe 47/48', N'Wintersemester 2047/48', N'Winter semester 2047/48',
        N'Das Wintersemester 2047/48 beginnt am 01.10.2047 und endet am 31.03.2048.',
        N'The winter semester 2047/48 starts on 01.10.2047 and ends on 31.03.2048.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 48', N'SuSe 48', N'Sommersemester 2048', N'Summer semester 2048',
        N'Das Sommersemester 2048 beginnt am 01.04.2048 und endet am 30.09.2048.',
        N'The summer semester 2048 starts on 01.04.2048 and ends on 30.09.2048.', 2);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'WiSe 48/49', N'WiSe 48/49', N'Wintersemester 2048/49', N'Winter semester 2048/49',
        N'Das Wintersemester 2048/49 beginnt am 01.10.2048 und endet am 31.03.2049.',
        N'The winter semester 2048/49 starts on 01.10.2048 and ends on 31.03.2049.', 1);
INSERT INTO semester (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en, term_id)
VALUES (N'SoSe 49', N'SuSe 49', N'Sommersemester 2049', N'Summer semester 2049',
        N'Das Sommersemester 2049 beginnt am 01.04.2049 und endet am 30.09.2049.',
        N'The summer semester 2049 starts on 01.04.2049 and ends on 30.09.2049.', 2);


-- Course type
INSERT INTO course_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en)
VALUES (N'LV', N'CO', N'Lehrveranstaltung', N'Course', N'', N'');
INSERT INTO course_type (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en)
VALUES (N'UEV', N'DUMMY_A', N'Übertragungsveranstaltung', N'DUMMY_A', N'', N'');
-- TODO: Change english fields to useful content


-- Course category
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'AG', N'DUMMY_A', N'Arbeitsgruppe', N'Working group', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'AA', N'DUMMY_B', N'Auf- und Abbau', N'Setup and removal', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'AT', N'DUMMY_C', N'Aufbaukurs Training', N'Advanced course training', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'AV', N'DUMMY_D', N'Aufbaukurs Vermittlung', N'Advanced course mediation', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'BS', N'DUMMY_E', N'Begleitendes Selbststudium', N'Associated private study', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'BE', N'DUMMY_F', N'Besprechung', N'Meeting', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'BU', N'DUMMY_G', N'Buchung', N'DUMMY_', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'DI', N'DUMMY_H', N'Disputation', N'Disputation', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'DS', N'DUMMY_I', N'Doktorandenseminar', N'Seminar for doctoral students', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'EV', N'DUMMY_J', N'Einführungsveranstaltung', N'Introductory course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'EW', N'DUMMY_K', N'Entwurf', N'Draft', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'EK', N'DUMMY_L', N'Exkursion', N'Field trip', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'EX', N'DUMMY_M', N'Experiment', N'Experiment', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'FS', N'DUMMY_N', N'Forschungsseminar', N'Research Seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'GA', N'DUMMY_O', N'Gastvortrag', N'Guest lecture', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'GK', N'DUMMY_P', N'Grundkurs', N'Basic course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'GÜ', N'DUMMY_Q', N'Gruppenübung', N'Group practice', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'GS', N'DUMMY_R', N'Gutachterseminar', N'Surveyor seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'HA', N'DUMMY_S', N'Habilitation', N'Habilitation', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'HS', N'DUMMY_T', N'Hauptseminar', N'Main seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'HÜ', N'DUMMY_U', N'Hörsaalübung', N'Lecture hall practice', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'IV', N'DUMMY_V', N'Integrierte Veranstaltung', N'Integrated course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'KE', N'DUMMY_W', N'Klausureinsicht', N'Post-exam review', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'KK', N'DUMMY_X', N'Klausurkorrektur', N'Correction of exams', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'KV', N'DUMMY_Y', N'Klausurvorbereitung', N'Exam preparation', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'KO', N'DUMMY_Z', N'Kolloquium', N'Colloquy', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'DUMMY_AA', N'DUMMY_AA', N'Kommunikation', N'Communication', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'KU', N'DUMMY_AB', N'Kurs', N'Course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'OS', N'DUMMY_AC', N'Oberseminar', N'Advanced seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'OV', N'DUMMY_AD', N'Orientierungsveranstaltung', N'Orientation course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PR', N'DUMMY_AE', N'Praktikum', N'Internship', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PL', N'DUMMY_AF', N'Praktikum in der Lehre', N'Internship teaching', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'DUMMY_AG', N'DUMMY_AG', N'Präsidium', N'Presidium', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PK', N'SE', N'Probeklausur', N'Sample exam', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PJ', N'DUMMY_AH', N'Projekt', N'Project', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PJG', N'DUMMY_AI', N'Projektgruppe', N'Project group', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PJS', N'DUMMY_AJ', N'Projektseminar', N'Project seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PM', N'DUMMY_AK', N'Promotion', N'PhD', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'DUMMY_AL', N'DUMMY_AL', N'Proseminar', N'Introductory seminar course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'PF', N'DUMMY_AM', N'Prüfung', N'Exam', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'SE', N'DUMMY_AN', N'Seminar', N'Seminar', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'SI', N'DUMMY_AO', N'Sitzung', N'Session', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'SR', N'DUMMY_AP', N'Sperrung', N'Closure', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'SK', N'DUMMY_AQ', N'Sprachkurs', N'TeachingLanguage course', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'SP', N'DUMMY_AR', N'Sprechstunde', N'Consultation hour', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'ST', N'DUMMY_AS', N'Stegreif', N'Impromptu', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'TG', N'DUMMY_AT', N'Tagung', N'Convention', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'TT', N'DUMMY_AU', N'Tutorium', N'Tutorial', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'UE', N'DUMMY_AV', N'Übung', N'Practice', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'VA', N'DUMMY_AW', N'Veranstaltung', N'Event', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'VB', N'DUMMY_AX', N'Vorbesprechung', N'Preliminary discussion', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'VL', N'DUMMY_AY', N'Vorlesung', N'Lecture', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'VU', N'DUMMY_AZ', N'Vorlesung und Übung', N'Lecture and practice', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'VO', N'DUMMY_BA', N'Vortrag', N'Talk', N'',
        N''); -- TODO: Change DUMMY fields to useful content
INSERT INTO course_category (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'WS', N'DUMMY_BB', N'Workshop', N'Workshop', N'', N'');
-- TODO: Change DUMMY fields to useful content


-- Teaching Language
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'de', N'ger', N'Deutsch', N'German');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'en', N'eng', N'Englisch', N'English');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'de/en', N'ger/eng', N'Deutsch/Englisch', N'German/English');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'ara', N'ara', N'Arabisch', N'Arabic');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'chi', N'zho', N'Chinesisch', N'Chinese');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'dan', N'dan', N'Dänisch', N'Danish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'fin', N'fin', N'Finnisch', N'Finnish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'fra', N'fre', N'Französisch', N'French');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'ita', N'ita', N'Italienisch', N'Italian');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'jpn', N'jpn', N'Japanisch', N'Japanese');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'kor', N'kor', N'Koreanisch', N'Korean');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'kur', N'kur', N'Kurdisch', N'Kurdish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'nld', N'dut', N'Niederländisch', N'Dutch');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'nor', N'nor', N'Norwegisch', N'Norwegian');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'pol', N'pol', N'Polnisch', N'Polish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'por', N'por', N'Portugiesisch', N'Portuguese');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'rus', N'rus', N'Russisch', N'Russian');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'swe', N'swe', N'Schwedisch', N'Swedish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'spa', N'spa', N'Spanisch', N'Spanish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'tur', N'tur', N'Türkisch', N'Turkish');
INSERT INTO teaching_language (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'vie', N'vie', N'Vietnamesisch', N'Vietnamese');


-- Title
INSERT INTO title (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                   description_en)
VALUES (N'TitleAbbrDe', N'TitleAbbrEn', N'TitleNameDe', N'TitleNameEn', N'TitleDescriptionDe',
        N'TitleDescriptionEn');
INSERT INTO title (name_de, name_en)
VALUES (N'Apl. Prof. Dr.', N'Apl. Prof. Dr.');
INSERT INTO title (name_de, name_en)
VALUES (N'Apl. Prof. Dr. phil.', N'Apl. Prof. Dr. phil.');
INSERT INTO title (name_de, name_en)
VALUES (N'Apl. Prof. Dr. rer. nat.', N'Apl. Prof. Dr. rer. nat.');
INSERT INTO title (name_de, name_en)
VALUES (N'Apl. Prof. Dr.-Ing.', N'Apl. Prof. Dr.-Ing.');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dipl.-Ing. Dipl.-Ök.', N'Dipl.-Ing. Dipl.-Ök.', N'Diplom-Ingenieur Diplom-Ökonom');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dipl.-Ing.', N'Dipl.-Ing.', N'Diplom-Ingenieur');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dipl.-Sportl.', N'Dipl.-Sportl.', N'Diplom-Sportlehrer');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dipl.-Sportwiss.', N'Dipl.-Sportwiss.', N'Diplom-Sportwissenschaftler');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dipl.-Wirt.-Inf.', N'Dipl.-Wirt.-Inf.', N'Diplom-Wirtschaftsinformatiker');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr.', N'Dr.', N'Doktor');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr.-Ing.', N'Dr.-Ing.', N'Doktor der Ingenieurwissenschaften');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. med.', N'Dr. med.', N'	Doktor der Medizin');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. päd.', N'Dr. päd.', N'Doktor der Erziehungswissenschaften');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. phil.', N'Dr. phil.', N'Doktor der Philosophie');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. phil. nat.', N'Dr. phil. nat.', N'Doktor der Naturwissenschaften');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. rer. nat.', N'Dr. rer. nat.', N'Doktor der Naturwissenschaften');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Dr. rer. pol.', N'Dr. rer. pol.',
        N'Sammelbegriff für Doktoren der Politikwissenschaft, Sozialwissenschaften, Staatswissenschaften oder Wirtschaftswissenschaften');
INSERT INTO title (name_de, name_en)
VALUES (N'Honorarprof. Dr.', N'Honorarprof. Dr.');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'M. A.', N'M. A.', N'Magister Artium');
INSERT INTO title (name_de, name_en)
VALUES (N'PD Dr.', N'PD Dr.');
INSERT INTO title (name_de, name_en)
VALUES (N'PD Dr.-Ing.', N'PD Dr.-Ing.');
INSERT INTO title (name_de, name_en, description_de)
VALUES (N'Prof.', N'Prof.', N'Professor');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. (em.) Dr.', N'Prof. (em.) Dr.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dipl.-Ing.', N'Prof. Dipl.-Ing.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr.', N'Prof. Dr.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. iur.', N'Prof. Dr. iur.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. jur.', N'Prof. Dr. jur.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. oec. publ.', N'Prof. Dr. oec. publ.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. päd.', N'Prof. Dr. päd.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. phil.', N'Prof. Dr. phil.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. phil. nat.', N'Prof. Dr. phil. nat.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. rer. nat.', N'Prof. Dr. rer. nat.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. rer. pol.', N'Prof. Dr. rer. pol.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. Sportwiss.', N'Prof. Dr. Sportwiss.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. techn.', N'Prof. Dr. techn.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. techn. Dr. h. c.', N'Prof. Dr. techn. Dr. h. c.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr.-Ing.', N'Prof. Dr.-Ing.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. med.', N'Prof. Dr. med.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Ph.D.', N'Prof. Ph.D.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. Dr.', N'Prof. Dr. Dr.');
INSERT INTO title (name_de, name_en)
VALUES (N'Dipl.-Wirt.-Ing.', N'Dipl.-Wirt.-Ing.');
INSERT INTO title (name_de, name_en)
VALUES (N'M. Sc.', N'M. Sc.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. med. Dr. habil.', N'Prof. Dr. med. Dr. habil.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. rer. nat. habil.', N'Prof. Dr. rer. nat. habil.');
INSERT INTO title (name_de, name_en)
VALUES (N'Prof. Dr. habil.', N'Prof. Dr. habil.');

-- Duration
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'1', N'1', N'1 Semester', N'1 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'2', N'2', N'2 Semester', N'2 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'3', N'3', N'3 Semester', N'3 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'4', N'4', N'4 Semester', N'4 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'5', N'5', N'5 Semester', N'5 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'6', N'6', N'6 Semester', N'6 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'7', N'7', N'7 Semester', N'7 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'8', N'8', N'8 Semester', N'8 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'9', N'9', N'9 Semester', N'9 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'10', N'10', N'10 Semester', N'10 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'11', N'11', N'11 Semester', N'11 Term', N'', N'');
INSERT INTO duration (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'12', N'12', N'12 Semester', N'12 Term', N'', N'');


-- Rotation
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'WiSe', N'WiSe', N'Wintersemester', N'Winter term', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'SoSe', N'SuSe', N'Sommersemester', N'Summer term', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en)
VALUES (N'Unregelmäßig', N'Irregular', N'Unregelmäßig', N'Irregular');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes Sem.', N'Every Sem.', N'Jedes Semester',
        N'Every Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 2. Sem.', N'Every 2. Sem.', N'Jedes 2. Semester',
        N'Every 2. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 3. Sem.', N'Every 3. Sem.', N'Jedes 3. Semester',
        N'Every 3. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 4. Sem.', N'Every 4. Sem.', N'Jedes 4. Semester',
        N'Every 4. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 5. Sem.', N'Every 5. Sem.', N'Jedes 5. Semester',
        N'Every 5. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 6. Sem.', N'Every 6. Sem.', N'Jedes 6. Semester',
        N'Every 6. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 7. Sem.', N'Every 7. Sem.', N'Jedes 7. Semester',
        N'Every 7. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 8. Sem.', N'Every 8. Sem.', N'Jedes 8. Semester',
        N'Every 8. Semester', N'', N'');
INSERT INTO rotation (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'Jedes 9. Sem.', N'Every 9. Sem.', N'Jedes 9. Semester',
        N'Every 9. Semester', N'', N'');


-- Grading system
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'STD', N'DUMMY_E', N'Standard', N'DUMMY_E', N'',
        N''); -- TODO: Change english fields to useful content
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'BST/NBST', N'DUMMY_A', N'Bestanden/Nicht bestanden', N'DUMMY_A', N'',
        N''); -- TODO: Change english fields to useful content
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'ECTS', N'DUMMY_B', N'ECTS Notensystem', N'DUMMY_B', N'',
        N''); -- TODO: Change english fields to useful content
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'GN', N'DUMMY_C', N'Genaue Note', N'DUMMY_C', N'',
        N''); -- TODO: Change english fields to useful content
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'PROM', N'DUMMY_D', N'Promotion', N'DUMMY_D', N'',
        N''); -- TODO: Change english fields to useful content
INSERT INTO grading_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'STATBWS', N'DUMMY_F', N'Statistikbewertungssystem', N'DUMMY_F', N'', N'');
-- TODO: Change english fields to useful content


-- Weighting Method
INSERT INTO weighting_method (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                              description_en)
VALUES (N'Prozent', N'Percent', N'Prozent', N'Percent', N'', N'');
INSERT INTO weighting_method (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                              description_en)
VALUES (N'Faktor', N'Factor', N'Faktor', N'Factor', N'', N'');


-- Responsibility
INSERT INTO responsibility (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'Hauptverantw.', N'Pic', N'Hauptverantwortliche Person', N'Person in Charge',
        N'Dies ist die hauptverantwortliche Person.', N'This is the person primarily responsible.');
INSERT INTO responsibility (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                            description_en)
VALUES (N'Assistent', N'Assistant', N'Assistent NameDe', N'Assistant NameEn',
        N'Assistent DescriptionDe', N'Assistant DescriptionEn');

-- Web_Language
INSERT INTO web_language (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          description_en)
VALUES (N'de', N'ger', N'Deutsch', N'German', N'Anzeige der Webseite in deutscher Sprache.',
        N'Display of the website in german language.');
INSERT INTO web_language (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          description_en)
VALUES (N'en', N'eng', N'Englisch', N'English',
        N'Anzeige der Webseite in englischer Sprache falls die Webseite in englischer Sprache verfügbar ist. Falls diese Webseite nicht in englisch verfügbar sein sollte, wird die deutsche Webseite angezeigt.',
        N'Display the website in english if the website is available in english. If this website is not available in english, the german website will be displayed.');


-- Sex
INSERT INTO sex (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                 description_en)
VALUES (N'u', N'u', N'Unbekannt', N'Not known', N'Unbekanntes Geschlecht', N'Unknown sex');
INSERT INTO sex (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                 description_en)
VALUES (N'm', N'm', N'Männlich', N'Male', N'Männliches Geschlecht', N'Male sex');
INSERT INTO sex (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                 description_en)
VALUES (N'w', N'f', N'Weiblich', N'Female', N'Weibliches Geschlecht', N'Female sex');


-- External_System
INSERT INTO external_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'TUCaN', N'TUCaN', N'TUCaN', N'TUCaN',
        N'Campus-Management-System der TU Darmstadt', N'Campus-Management System of TU Darmstadt');
INSERT INTO external_system (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                             description_en)
VALUES (N'TUDa-LDAP', N'TUDa-LDAP', N'TUDa-LDAP', N'TUDa-LDAP',
        N'Zentraler Verzeichnisdienst der TU Darmstadt',
        N'Central directory information service of TU Darmstadt');

-- External_Data
INSERT INTO external_data (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'Modul', N'Module', N'Modul', N'Module',
        N'Informationen zu den Modulen und Modulbeschreibungen für die Lehrveranstaltungen.',
        N'Module information and module descriptions for courses.');
INSERT INTO external_data (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'Kurs', N'Course', N'Kurs', N'Course',
        N'Informationen zu den Kursen und Kursbeschreibungen für die Lehrveranstaltungen.',
        N'Course information and course descriptions for courses.');
INSERT INTO external_data (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'Prüfung', N'Exam', N'Prüfung', N'Exam',
        N'Informationen zu den Prüfungen für die Lehrveranstaltungen.',
        N'Information about the exams for the courses.');
INSERT INTO external_data (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                           description_en)
VALUES (N'TUDa-Account', N'TUDa-Account', N'TUDa Benutzerkonto', N'TUDa account',
        N'Informationen zum TUDa Benutzerkonto verknüpft über die TU-ID.',
        N'Information about the TUDa user account linked via the TU-ID.');

-----------------------------------------------------------
-- Other tables mostly with foreign keys
-----------------------------------------------------------

-- Organization
-- TU Darmstadt
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          cost_center)
VALUES (N'TUDa', N'TUDa', N'Technische Universität Darmstadt', N'Technical University of Darmstadt',
        N'', N'*');


-- Fachbereich ETiT
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          description_en, cost_center, parent_id)
VALUES (N'FB18', N'FB18', N'Fachbereich Elektro- und Informationstechnik',
        N'Department of Electrical Engineering and Information Technology',
        N'Fachbereich für Elektrotechnik, der schon seit 1882 besteht.',
        N'Department of Electrical Engineering and Information Technology, which was found in 1882.',
        N'18*', 1);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 1', N'FB 1', N'Fachbereich Rechts- und Wirtschaftswissenschaften',
        N'Department of Law and Economics', N'01*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2', N'FB 2', N'Fachbereich Gesellschafts- und Geschichtswissenschaften',
        N'Department of History and Social Sciences', N'02*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 3', N'FB 3', N'Fachbereich Humanwissenschaften', N'Department of Human Sciences',
        N'03*',
        1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 4', N'FB 4', N'Fachbereich Mathematik', N'Department of Mathematics', N'04*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 5', N'FB 5', N'Fachbereich Physik', N'Department of Physics', N'05*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 7', N'FB 7', N'Fachbereich Chemie', N'Department of Chemistry', N'07*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 10', N'FB 10', N'Fachbereich Biologie', N'Department of Biology', N'10*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 11', N'FB 11', N'Fachbereich Material- und Geowissenschaften',
        N'Department of Materials and Earth Sciences', N'11*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 13', N'FB 13', N'Fachbereich Bau- und Umweltingenieurwissenschaften',
        N'Department of Civil and Environmental Engineering', N'13*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 15', N'FB 15', N'Fachbereich Architektur', N'Department of Architecture', N'15*', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 16', N'FB 16', N'Fachbereich Maschinenbau', N'Department of Mechanical Engineering',
        N'16*',
        1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 20', N'FB 20', N'Fachbereich Informatik', N'Department of Computer Science', N'20*',
        1);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'SB iST', N'SB iST', N'Studienbereich Informationssystemtechnik',
        N'Field of Study Information System Technology', N'?', 2);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'SB MEC', N'SB MEC', N'Studienbereich Mechanik', N'Field of Study Mechanics', N'?', 2);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'SB CE', N'SB CE', N'Studienbereich Computational Engineering',
        N'Field of Study Computational Engineering', N'?', 1);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'SB ESE', N'SB ESE', N'Studienbereich Energy Science and Engineering',
        N'Field of Study Energy Science and Engineering', N'?', 2);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'SPZ', N'SPZ', N'Sprachenzentrum', N'Language Resource Center', N'?', 1);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Geschichte', N'FB 2 History', N'FB 2 / Institut für Geschichte',
        N'FB 2 / Institute of History', N'?', 4);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Philosophie', N'FB 2 Philosophy', N'FB 2 / Institut für Philosophie',
        N'FB 2 / Institute of Philosophy', N'?', 4);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Polikitwiss.', N'FB 2 Political Sci.', N'FB 2 / Institut für Politikwissenschaft',
        N'FB 2 / Institute of Political Science', N'?', 4);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Soziologie', N'FB 2 Sociology', N'FB 2 / Institut für Soziologie',
        N'FB 2 / Institute of Sociology', N'?', 4);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Sprach./Lit.', N'FB 2 Ling./Lit.',
        N'FB 2 / Institut für Sprach- und Literaturwissenschaft',
        N'FB 2 / Institute of Linguistics and Literary Studies', N'?', 4);

INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 2 Theologie', N'FB 2 Theology',
        N'FB 2 / Institut für Theologie und Sozialethik',
        N'FB 2 / Institute of Theology and Social Ethics', N'?', 4);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 3 Pädagogik', N'FB 3 Pedagogy',
        N'FB 3 / Institut für Allgemeine Pädagogik und Berufspädagogik',
        N'FB 3 / Institute of General and Vocational Pedagogy', N'?', 5);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 3 Psychologie', N'FB 3 Psychology', N'FB 3 / Institut für Psychologie',
        N'FB 3 / Institute of Psychology', N'?', 5);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 3 Sport', N'FB 3 Sport', N'FB 3 / Institut für Sportwissenschaft',
        N'FB 3 / Institute of Sport Science', N'?', 5);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 11 Materialwiss.', N'FB 11 Material Sci.',
        N'FB 11 / Institut für Materialwissenschaft',
        N'FB 11 / Institute of Materials Science', N'?', 10);
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, cost_center,
                          parent_id)
VALUES (N'FB 11 Geowiss.', N'FB 11 Geosci.', N'FB 11 / Institut für Angewandte Geowissenschaften',
        N'FB 11 / Institute of Applied Geosciences', N'?', 10);

-- Dekanat FB 18
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          description_en, cost_center, parent_id)
VALUES (N'DEK18', N'DEK18', N'Dekanat des FB Elektro- und Informationstechnik',
        N'Deans Office of department Electrical Engineering and Information Technology',
        N'Dekan, Prodekan, Studiendekan, Geschäftsführung, Servicezentrum (Studienbüro), Forschungsangelegenheiten, Kommunikation und Organisationsentwicklung.',
        N'Dean, Vice-Dean, Dean of Students, Department Management, the Service Centre, Research Affairs, the Committee of Communication, and the Committee of Organisational Development.',
        N'1800*', 2);
-- Fachschaft etit
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, parent_id)
VALUES (N'FSetit', N'FSetit', N'Fachschaft etit', N'Student council etit', 2);
-- FG RMR
INSERT INTO organization (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                          description_en, cost_center, parent_id)
VALUES (N'FG RMR', N'FG RMR', N'Fachgebiet Regelungsmethoden und Robotik',
        N'Institute of Control Methods and Robotics', N'', N'', N'1804*', 2);


-- Role
INSERT INTO role (protected, uuid, abbreviation_de, abbreviation_en, name_de, name_en,
                  description_de, description_en, organization_id)
VALUES (0, N'role.role-abbr-en-1', N'RoleAbbrDe1', N'RoleAbbrEn1', N'RoleNameDe1', N'RoleNameEn1',
        N'RoleDescriptionDe1', N'RoleDescriptionEn1', 1);
INSERT INTO role (protected, uuid, abbreviation_de, abbreviation_en, name_de, name_en,
                  description_de, description_en, organization_id)
VALUES (1, N'role.role-abbr-en-2', N'RoleAbbrDe2', N'RoleAbbrEn2', N'RoleNameDe2', N'RoleNameEn2',
        N'RoleDescriptionDe2', N'RoleDescriptionEn2', 2);
INSERT INTO role (protected, uuid, abbreviation_de, abbreviation_en, name_de, name_en,
                  description_de, description_en, organization_id)
VALUES (0, N'role.role-abbr-en-3', N'RoleAbbrDe3', N'RoleAbbrEn3', N'RoleNameDe3', N'RoleNameEn3',
        N'RoleDescriptionDe3', N'RoleDescriptionEn3', 1);

-- role_has_role
INSERT INTO role_has_role (parent_role_id, child_role_id)
VALUES (1, 2);
INSERT INTO role_has_role (parent_role_id, child_role_id)
VALUES (1, 3);
INSERT INTO role_has_role (parent_role_id, child_role_id)
VALUES (2, 3);

-- permission
INSERT INTO permission (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                        description_en)
VALUES (N'permission.perm1', N'PermAbbrDe1', N'PermAbbrEn1', N'PermNameDe1', N'PermNameEn1',
        N'PermDescriptionDe1', N'PermDescriptionEn1');
INSERT INTO permission (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                        description_en)
VALUES (N'permission.perm2', N'PermAbbrDe2', N'PermAbbrEn2', N'PermNameDe2', N'PermNameEn2',
        N'PermDescriptionDe2', N'PermDescriptionEn2');
INSERT INTO permission (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                        description_en)
VALUES (N'permission.perm3', N'PermAbbrDe3', N'PermAbbrEn3', N'PermNameDe3', N'PermNameEn3',
        N'PermDescriptionDe3', N'PermDescriptionEn3');

-- resource
INSERT INTO resource (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'resource.res1', N'ResAbbrDe1', N'ResAbbrEn1', N'ResNameDe1', N'ResNameEn1',
        N'ResDescriptionDe1', N'ResDescriptionEn1');
INSERT INTO resource (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'resource.res2', N'ResAbbrDe2', N'ResAbbrEn2', N'ResNameDe2', N'ResNameEn2',
        N'ResDescriptionDe2', N'ResDescriptionEn2');
INSERT INTO resource (uuid, abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                      description_en)
VALUES (N'resource.res3', N'ResAbbrDe3', N'ResAbbrEn3', N'ResNameDe3', N'ResNameEn3',
        N'ResDescriptionDe3', N'ResDescriptionEn3');

-- permission_has_resource
INSERT INTO permission_has_resource (permission_id, resource_id)
VALUES (1, 1);
INSERT INTO permission_has_resource (permission_id, resource_id)
VALUES (2, 1);
INSERT INTO permission_has_resource (permission_id, resource_id)
VALUES (3, 2);

-- role_has_permission_and_resource
INSERT INTO role_has_permission_and_resource (role_id, permission_id, resource_id, organization_id)
VALUES (1, 1, 1, 1);
INSERT INTO role_has_permission_and_resource (role_id, permission_id, resource_id, organization_id)
VALUES (1, 2, 1, 2);
INSERT INTO role_has_permission_and_resource (role_id, permission_id, resource_id)
VALUES (2, 1, 1);
INSERT INTO role_has_permission_and_resource (role_id, permission_id, resource_id, organization_id)
VALUES (3, 3, 2, 1);

-- Setting_Key
INSERT INTO setting_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en, language_dependent,
                         keyword)
VALUES (N'VFL Version', N'VFL Version', N'VFL Version', N'VFL Version', N'VFL Release Version.',
        N'VFL release version.', 0,
        N'application.vfl.version');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Version Datenbank-Schema', N'Version database schema',
        N'Internes Schema Version der Datenbank.', N'Internal schema version of the database.', 0,
        N'application.database.schema.version');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Datenbank Name', N'Database name', N'Name der Datenbank.', N'Name of the database.', 0,
        N'application.database.name');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Datenbank Produkt Version', N'Database product version',
        N'Produkt Version der Datenbank.',
        N'Product version of the database.', 0, N'application.database.version');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Datenbank Produkt-Update-Ebene', N'Database product update level',
        N'Update Level der jeweiligen Produktversion der Datenbank.',
        N'Update level of the respective product version of the database.', 0,
        N'application.database.update-level');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'LaTeX-Web-API URL', N'LaTeX-Web-API URL',
        N'URL ohne Protokoll und Port für die LaTeX-Web-API.',
        N'URL without protocol and port for the LaTeX web API.', 0,
        N'external_api.latex-web-api.url');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'LaTeX-Web-API Protokoll', N'LaTeX-Web-API Protocol', N'Protokoll für die LaTeX-Web-API.',
        N'Protocol for the LaTeX web API.', 0, N'external_api.latex-web-api.protocol');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'LaTeX-Web-API Port', N'LaTeX-Web-API Port', N'Port für die LaTeX-Web-API.',
        N'Port of the LaTeX web API.', 0, N'external_api.latex-web-api.port');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'LaTeX-Web-API Version', N'LaTeX-Web-API version', N'Version der LaTeX-Web-API.',
        N'Version of the LaTeX web API.', 0, N'external_api.latex-web-api.version');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'TUDa-LDAP URL', N'TUDa-LDAP URL', N'URL des TUDa-LDAP Servers ohne Protokoll oder Port.',
        N'URL of the TUDa-LDAP server without protocol or port.', 0, N'external_api.tuda-ldap.url');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'TUDa-LDAP Protokoll', N'TUDa-LDAP Protocol', N'Protokoll des TUDa-LDAP Servers.',
        N'Protocol of the TUDa-LDAP server.', 0, N'external_api.tuda-ldap.protocol');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'TUDa-LDAP Port', N'TUDa-LDAP Port', N'Port des TUDa-LDAP Servers.',
        N'Port of the TUDa-LDAP server.',
        0, N'external_api.tuda-ldap.port');
INSERT INTO setting_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en, language_dependent, keyword)
VALUES (N'TUCaN Module', N'TUCaN Modules', N'Module aus TUCaN', N'Modules from TUCaN',
        N'Absoluter Pfad zur Datei mit allen Modulen in TUCaN (Export aus TUCaN).',
        N'Absolute path to the file with all modules in TUCaN (export from TUCaN).', 0,
        N'external_api.tucan-import.modules.path');
INSERT INTO setting_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en, language_dependent, keyword)
VALUES (N'TUCaN Kurse', N'TUCaN Courses', N'Kurse aus TUCaN', N'Courses from TUCaN',
        N'Absoluter Pfad zur Datei mit allen Kursen in TUCaN (Export aus TUCaN).',
        N'Absolute path to the file with all courses in TUCaN (export from TUCaN).', 0,
        N'external_api.tucan-import.courses.path');
INSERT INTO setting_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en, language_dependent, keyword)
VALUES (N'TUCaN Prüfungen', N'TUCaN Exams', N'Prüfungen aus TUCaN', N'Exams from TUCaN',
        N'Absoluter Pfad zur Datei mit allen Prüfungen in TUCaN (Export aus TUCaN).',
        N'Absolute path to the file with all exams in TUCaN (export from TUCaN).', 0,
        N'external_api.tucan-import.exams.path');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'TUCaN Import Zeitplan/Schedule', N'TUCaN import schedule',
        N'TUCaN Import Zeitplan/Schedule', N'TUCaN import schedule', 0,
        N'application.vfl.tucan_import_schedule');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Health Check Zeitplan/Schedule', N'Health Check schedule',
        N'Health Check Zeitplan/Schedule', N'Health Check schedule', 0,
        N'application.vfl.health_check_schedule');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Export Curriculum Scheme Zeitplan/Schedule', N'export curriculum scheme schedule',
        N'Export Curriculum Scheme Zeitplan/Schedule', N'export curriculum scheme schedule', 0,
        N'application.vfl.export_scheme_schedule');
INSERT INTO setting_key (name_de, name_en, description_de, description_en, language_dependent,
                         keyword)
VALUES (N'Ergebnispfad des automatischen Exports der Datenquelle für die Module.',
        N'Result path of the automatic curriculum scheme export.',
        N'Ergebnispfad des automatischen Exports der Datenquelle für die Module.',
        N'Result path of the automatic curriculum scheme export.', 0,
        N'application.vfl.path_scheduled_export_curriculum_scheme');
INSERT INTO setting_key (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                         description_en, language_dependent,
                         keyword)
VALUES (N'Akzentfarbe', N'Accent color', N'Akzentfarbe', N'Accent color',
        N'Akzentfarbe für die Dokumente angelehnt an die Corporate Design Farben der TUDa. Die Farben werden über den HEX-Code kodiert.',
        N'Accent color for the documents based on the corporate design colors of TUDa. The colors are coded via the HEX code.',
        0, N'document.color.accent');

-- Setting_value
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'0.1.1', 1, 1);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'0.1.0', 1, 2);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'ms-sql', 1, 3);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'15.0', 1, 4);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'CU9', 1, 5);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'localhost', 1, 6);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'http', 1, 7);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'8080', 1, 8);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'1.0.0', 1, 9);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'ldap.tu-darmstadt.de', 1, 10);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'ldaps', 1, 11);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'636', 1, 12);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'/media/hrzshare/XM1_Module_de_en.xml', 1, 13);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'/media/hrzshare/XM2_Kurs', 1, 14);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'/media/hrzshare/XM3_Prue', 1, 15);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'0 30 6 ? * * *', 1, 16);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'0 45 6 ? * * *', 1, 17);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'0 30 6 ? * * *', 1, 18);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'/var/vfl/export', 1, 19);
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'#B90F22', 1, 20)
INSERT INTO setting_value (content_de, organization_id, key_id)
VALUES (N'#00689D', 2, 20);

-- Feature
INSERT INTO feature (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                     description_en)
VALUES (N'VFL_Einstellungen', N'VFL_Settings', N'Generelle Anwendungseinstellungen',
        N'General application settings', N'Grundlegende Einstellungen für VFL.',
        N'General settings for VFL.');
INSERT INTO feature (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                     description_en)
VALUES (N'Webseite', N'Webpage', N'Webseiteneinstellungen',
        N'Web page settings', N'Grundlegende Einstellungen für VFL Webseite.',
        N'General settings for VFL web page.');
INSERT INTO feature (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                     description_en)
VALUES (N'Ext_API', N'Ext_API', N'Externe Schnittstellen', N'External interfaces',
        N'Einstellungen für die externen Schnittstellen (APIs) die von VFL verwendet werden.',
        N'Settings for the feature interfaces (APIs) used by VFL.');
INSERT INTO feature (abbreviation_de, abbreviation_en, name_de, name_en, description_de,
                     description_en)
VALUES (N'Dok_Einstellungen', N'Doc_Settings', N'Generelle Dokumenteneinstellungen',
        N'General document settings',
        N'Grundlegende Einstellungen für die Dokumente.',
        N'General settings for the documents.');


-- Feature_has_setting_key
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 1);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 2);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 3);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 4);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 5);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 6);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 7);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 8);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 9);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 10);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 11);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (3, 12);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 13);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 14);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 15);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 16);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 17);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 18);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (1, 19);
INSERT INTO feature_has_setting_key (feature_id, setting_key_id)
VALUES (4, 20);

