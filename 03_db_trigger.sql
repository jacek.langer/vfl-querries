USE testvfl;

-- -----------------------------------------------------
-- Trigger for master data tables
-- -----------------------------------------------------

-- Trigger for table country
CREATE TRIGGER country_updated_at
	ON country
	AFTER UPDATE
	AS
	UPDATE country
	SET updated_at = SYSDATETIMEOFFSET()
	FROM country c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table city
CREATE TRIGGER city_updated_at
	ON city
	AFTER UPDATE
	AS
	UPDATE city
	SET updated_at = SYSDATETIMEOFFSET()
	FROM city c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table file_format
CREATE TRIGGER file_format_updated_at
	ON file_format
	AFTER UPDATE
	AS
	UPDATE file_format
	SET updated_at = SYSDATETIMEOFFSET()
	FROM file_format f
		INNER JOIN inserted i ON f.id = i.id;

-- Trigger for table exam_category
CREATE TRIGGER exam_category_updated_at
	ON exam_category
	AFTER UPDATE
	AS
	UPDATE exam_category
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_category e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_type
CREATE TRIGGER exam_type_updated_at
	ON exam_type
	AFTER UPDATE
	AS
	UPDATE exam_type
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_type e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table term
CREATE TRIGGER term_updated_at
	ON term
	AFTER UPDATE
	AS
	UPDATE term
	SET updated_at = SYSDATETIMEOFFSET()
	FROM term t
		INNER JOIN inserted i ON t.id = i.id;

-- Trigger for table semester
CREATE TRIGGER semester_updated_at
	ON semester
	AFTER UPDATE
	AS
	UPDATE semester
	SET updated_at = SYSDATETIMEOFFSET()
	FROM semester s
		INNER JOIN inserted i ON s.id = i.id;

-- Trigger for table course_type
CREATE TRIGGER course_type_updated_at
	ON course_type
	AFTER UPDATE
	AS
	UPDATE course_type
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_type c
			 INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table course_category
CREATE TRIGGER course_category_updated_at
	ON course_category
	AFTER UPDATE
	AS
	UPDATE course_category
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_category c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table document_area
CREATE TRIGGER document_area_updated_at
	ON document_area
	AFTER UPDATE
	AS
	UPDATE document_area
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_area d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table responsibility
CREATE TRIGGER responsibility_updated_at
	ON responsibility
	AFTER UPDATE
	AS
	UPDATE responsibility
	SET updated_at = SYSDATETIMEOFFSET()
	FROM responsibility r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table web_language
CREATE TRIGGER web_language_updated_at
	ON web_language
	AFTER UPDATE
	AS
	UPDATE web_language
	SET updated_at = SYSDATETIMEOFFSET()
	FROM web_language w
		INNER JOIN inserted i ON w.id = i.id;

-- Trigger for table sex
CREATE TRIGGER sex_updated_at
	ON sex
	AFTER UPDATE
	AS
	UPDATE sex
	SET updated_at = SYSDATETIMEOFFSET()
	FROM sex s
		INNER JOIN inserted i ON s.id = i.id;

-- Trigger for table teaching_language
CREATE TRIGGER teaching_language_updated_at
	ON teaching_language
	AFTER UPDATE
	AS
	UPDATE teaching_language
	SET updated_at = SYSDATETIMEOFFSET()
	FROM teaching_language t
		INNER JOIN inserted i ON t.id = i.id;

-- Trigger for table title
CREATE TRIGGER title_updated_at
	ON title
	AFTER UPDATE
	AS
	UPDATE title
	SET updated_at = SYSDATETIMEOFFSET()
	FROM title t
		INNER JOIN inserted i ON t.id = i.id;

-- Trigger for table duration
CREATE TRIGGER duration_updated_at
	ON duration
	AFTER UPDATE
	AS
	UPDATE duration
	SET updated_at = SYSDATETIMEOFFSET()
	FROM duration d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table rotation
CREATE TRIGGER rotation_updated_at
	ON rotation
	AFTER UPDATE
	AS
	UPDATE rotation
	SET updated_at = SYSDATETIMEOFFSET()
	FROM rotation r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table grading_system
CREATE TRIGGER grading_system_updated_at
	ON grading_system
	AFTER UPDATE
	AS
	UPDATE grading_system
	SET updated_at = SYSDATETIMEOFFSET()
	FROM grading_system g
		INNER JOIN inserted i ON g.id = i.id;

-- Trigger for table weighting_method
CREATE TRIGGER weighting_method_updated_at
	ON weighting_method
	AFTER UPDATE
	AS
	UPDATE weighting_method
	SET updated_at = SYSDATETIMEOFFSET()
	FROM weighting_method w
		INNER JOIN inserted i ON w.id = i.id;

-- Trigger for table external_system
CREATE TRIGGER external_system_updated_at
	ON external_system
	AFTER UPDATE
	AS
	UPDATE external_system
	SET updated_at = SYSDATETIMEOFFSET()
	FROM external_system e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table external_data
CREATE TRIGGER external_data_updated_at
	ON external_data
	AFTER UPDATE
	AS
	UPDATE external_data
	SET updated_at = SYSDATETIMEOFFSET()
	FROM external_data e
		INNER JOIN inserted i ON e.id = i.id;


-- -----------------------------------------------------
-- Other tables
-- -----------------------------------------------------

-- Trigger for table person_preference
CREATE TRIGGER person_preference_updated_at
	ON person_preference
	AFTER UPDATE
	AS
	UPDATE person_preference
	SET updated_at = SYSDATETIMEOFFSET()
	FROM person_preference p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table person_profile
CREATE TRIGGER person_profile_updated_at
	ON person_profile
	AFTER UPDATE
	AS
	UPDATE person_profile
	SET updated_at = SYSDATETIMEOFFSET()
	FROM person_profile p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table person
CREATE TRIGGER person_updated_at
	ON person
	AFTER UPDATE
	AS
	UPDATE person
	SET updated_at = SYSDATETIMEOFFSET()
	FROM person p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table account_local
CREATE TRIGGER account_local_updated_at
	ON account_local
	AFTER UPDATE
	AS
	UPDATE account_local
	SET updated_at = SYSDATETIMEOFFSET()
	FROM account_local a
		INNER JOIN inserted i ON a.id = i.id;

-- Trigger for table account_tuda
CREATE TRIGGER account_tuda_updated_at
	ON account_tuda
	AFTER UPDATE
	AS
	UPDATE account_tuda
	SET updated_at = SYSDATETIMEOFFSET()
	FROM account_tuda a
		INNER JOIN inserted i ON a.id = i.id;

-- Trigger for table organization
CREATE TRIGGER organization_updated_at
	ON organization
	AFTER UPDATE
	AS
	UPDATE organization
	SET updated_at = SYSDATETIMEOFFSET()
	FROM organization o
		INNER JOIN inserted i ON o.id = i.id;

-- Trigger for table organization_has_person
CREATE TRIGGER organization_has_person_updated_at
	ON organization_has_person
	AFTER UPDATE
	AS
	UPDATE organization_has_person
	SET updated_at = SYSDATETIMEOFFSET()
	FROM organization_has_person o
		INNER JOIN inserted i ON o.id = i.id;

-- Trigger for table role
CREATE TRIGGER role_updated_at
	ON role
	AFTER UPDATE
	AS
	UPDATE role
	SET updated_at = SYSDATETIMEOFFSET()
	FROM role r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table role_has_role
CREATE TRIGGER role_has_role_updated_at
	ON role_has_role
	AFTER UPDATE
	AS
	UPDATE role_has_role
	SET updated_at = SYSDATETIMEOFFSET()
	FROM role_has_role r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table account_has_role
CREATE TRIGGER account_has_role_updated_at
	ON account_has_role
	AFTER UPDATE
	AS
	UPDATE account_has_role
	SET updated_at = SYSDATETIMEOFFSET()
	FROM account_has_role a
		INNER JOIN inserted i ON a.id = i.id;

-- Trigger for table permission
CREATE TRIGGER permission_updated_at
	ON permission
	AFTER UPDATE
	AS
	UPDATE permission
	SET updated_at = SYSDATETIMEOFFSET()
	FROM permission p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table resource
CREATE TRIGGER resource_updated_at
	ON resource
	AFTER UPDATE
	AS
	UPDATE resource
	SET updated_at = SYSDATETIMEOFFSET()
	FROM resource r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table permission_has_resource
CREATE TRIGGER permission_has_resource_updated_at
	ON permission_has_resource
	AFTER UPDATE
	AS
	UPDATE permission_has_resource
	SET updated_at = SYSDATETIMEOFFSET()
	FROM permission_has_resource p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table role_has_permission_and_resource
CREATE TRIGGER role_has_permission_and_resource_updated_at
	ON role_has_permission_and_resource
	AFTER UPDATE
	AS
	UPDATE role_has_permission_and_resource
	SET updated_at = SYSDATETIMEOFFSET()
	FROM role_has_permission_and_resource r
		INNER JOIN inserted i ON r.id = i.id;

-- Trigger for table module_base
CREATE TRIGGER module_base_updated_at
	ON module_base
	AFTER UPDATE
	AS
	UPDATE module_base
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_base m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table module_detail
CREATE TRIGGER module_detail_updated_at
	ON module_detail
	AFTER UPDATE
	AS
	UPDATE module_detail
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_detail m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table module_version
CREATE TRIGGER module_version_updated_at
	ON module_version
	AFTER UPDATE
	AS
	UPDATE module_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_version m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table module_base_has_module_version
CREATE TRIGGER module_base_has_module_version_updated_at
	ON module_base_has_module_version
	AFTER UPDATE
	AS
	UPDATE module_base_has_module_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_base_has_module_version m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table module_version_has_responsible_person
CREATE TRIGGER module_version_has_responsible_person_updated_at
	ON module_version_has_responsible_person
	AFTER UPDATE
	AS
	UPDATE module_version_has_responsible_person
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_version_has_responsible_person m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table course_base
CREATE TRIGGER course_base_updated_at
	ON course_base
	AFTER UPDATE
	AS
	UPDATE course_base
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_base c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table course_detail
CREATE TRIGGER course_detail_updated_at
	ON course_detail
	AFTER UPDATE
	AS
	UPDATE course_detail
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_detail c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table course_version
CREATE TRIGGER course_version_updated_at
	ON course_version
	AFTER UPDATE
	AS
	UPDATE course_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_version c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table course_base_has_course_version
CREATE TRIGGER course_base_has_course_version_updated_at
	ON course_base_has_course_version
	AFTER UPDATE
	AS
	UPDATE course_base_has_course_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_base_has_course_version c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table course_version_has_responsible_person
CREATE TRIGGER course_version_has_responsible_person_updated_at
	ON course_version_has_responsible_person
	AFTER UPDATE
	AS
	UPDATE course_version_has_responsible_person
	SET updated_at = SYSDATETIMEOFFSET()
	FROM course_version_has_responsible_person c
		INNER JOIN inserted i ON c.id = i.id;

-- Trigger for table module_version_has_course_version
CREATE TRIGGER module_version_has_course_version_updated_at
	ON module_version_has_course_version
	AFTER UPDATE
	AS
	UPDATE module_version_has_course_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_version_has_course_version m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table exam_base
CREATE TRIGGER exam_base_updated_at
	ON exam_base
	AFTER UPDATE
	AS
	UPDATE exam_base
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_base e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_detail
CREATE TRIGGER exam_detail_updated_at
	ON exam_detail
	AFTER UPDATE
	AS
	UPDATE exam_detail
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_detail e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_version
CREATE TRIGGER exam_version_updated_at
	ON exam_version
	AFTER UPDATE
	AS
	UPDATE exam_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_version e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_base_has_exam_version
CREATE TRIGGER exam_base_has_exam_version_updated_at
	ON exam_base_has_exam_version
	AFTER UPDATE
	AS
	UPDATE exam_base_has_exam_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_base_has_exam_version e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_version_has_tucan_exam_id
CREATE TRIGGER exam_version_has_tucan_exam_id_updated_at
	ON exam_version_has_tucan_exam_id
	AFTER UPDATE
	AS
	UPDATE exam_version_has_tucan_exam_id
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_version_has_tucan_exam_id e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table exam_version_has_responsible_person
CREATE TRIGGER exam_version_has_responsible_person_updated_at
	ON exam_version_has_responsible_person
	AFTER UPDATE
	AS
	UPDATE exam_version_has_responsible_person
	SET updated_at = SYSDATETIMEOFFSET()
	FROM exam_version_has_responsible_person e
		INNER JOIN inserted i ON e.id = i.id;

-- Trigger for table module_version_has_exam_version
CREATE TRIGGER module_version_has_exam_version_updated_at
	ON module_version_has_exam_version
	AFTER UPDATE
	AS
	UPDATE module_version_has_exam_version
	SET updated_at = SYSDATETIMEOFFSET()
	FROM module_version_has_exam_version m
		INNER JOIN inserted i ON m.id = i.id;

-- Trigger for table document_category
CREATE TRIGGER document_category_updated_at
	ON document_category
	AFTER UPDATE
	AS
	UPDATE document_category
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_category d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table document_type
CREATE TRIGGER document_type_updated_at
	ON document_type
	AFTER UPDATE
	AS
	UPDATE document_type
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_type d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table document_template
CREATE TRIGGER document_template_updated_at
	ON document_template
	AFTER UPDATE
	AS
	UPDATE document_template
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_template d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table document_template_has_template
CREATE TRIGGER document_template_has_template_updated_at
	ON document_template_has_template
	AFTER UPDATE
	AS
	UPDATE document_template_has_template
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_template_has_template d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table part_key
CREATE TRIGGER part_key_updated_at
	ON part_key
	AFTER UPDATE
	AS
	UPDATE part_key
	SET updated_at = SYSDATETIMEOFFSET()
	FROM part_key p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table part_value
CREATE TRIGGER part_value_updated_at
	ON part_value
	AFTER UPDATE
	AS
	UPDATE part_value
	SET updated_at = SYSDATETIMEOFFSET()
	FROM part_value p
		INNER JOIN inserted i ON p.id = i.id;

-- Trigger for table document_template_has_part_key
CREATE TRIGGER document_template_has_part_key_updated_at
	ON document_template_has_part_key
	AFTER UPDATE
	AS
	UPDATE document_template_has_part_key
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_template_has_part_key d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table document_template_has_file_format
CREATE TRIGGER document_template_has_file_format_updated_at
	ON document_template_has_file_format
	AFTER UPDATE
	AS
	UPDATE document_template_has_file_format
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_template_has_file_format d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table setting_key
CREATE TRIGGER setting_key_updated_at
	ON setting_key
	AFTER UPDATE
	AS
	UPDATE setting_key
	SET updated_at = SYSDATETIMEOFFSET()
	FROM setting_key s
		INNER JOIN inserted i ON s.id = i.id;

-- Trigger for table setting_value
CREATE TRIGGER setting_value_updated_at
	ON setting_value
	AFTER UPDATE
	AS
	UPDATE setting_value
	SET updated_at = SYSDATETIMEOFFSET()
	FROM setting_value s
		INNER JOIN inserted i ON s.id = i.id;

-- Trigger for table document_template_has_setting_key
CREATE TRIGGER document_template_has_setting_key_updated_at
	ON document_template_has_setting_key
	AFTER UPDATE
	AS
	UPDATE document_template_has_setting_key
	SET updated_at = SYSDATETIMEOFFSET()
	FROM document_template_has_setting_key d
		INNER JOIN inserted i ON d.id = i.id;

-- Trigger for table feature
CREATE TRIGGER feature_updated_at
	ON feature
	AFTER UPDATE
	AS
	UPDATE feature
	SET updated_at = SYSDATETIMEOFFSET()
	FROM feature s
		INNER JOIN inserted i ON s.id = i.id;

-- Trigger for table feature_has_setting_key
CREATE TRIGGER feature_has_setting_key_updated_at
	ON feature_has_setting_key
	AFTER UPDATE
	AS
	UPDATE feature_has_setting_key
	SET updated_at = SYSDATETIMEOFFSET()
	FROM feature_has_setting_key f
		INNER JOIN inserted i ON f.id = i.id;


-- Trigger for table data_import_log
CREATE TRIGGER data_import_log_updated_at
	ON data_import_log
	AFTER UPDATE
	AS
	UPDATE data_import_log
	SET updated_at = SYSDATETIMEOFFSET()
	FROM data_import_log d
		INNER JOIN inserted i ON d.id = i.id;