USE testvfl;

-- Module_Base
INSERT INTO module_base (tucan_module_id, activated_at, organization_id)
VALUES (123456789012345, N'2020-01-17 14:11:00.0000000 +01:00', 1);
INSERT INTO module_base (tucan_module_id, activated_at, deactivated_at, organization_id)
VALUES (234567890123456, N'2020-03-1 13:22:00.0000000 +01:00',
        N'2020-03-19 19:22:00.0000000 +01:00',
        1);


-- Module_Detail
INSERT INTO module_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, description_de,
                           description_en, credit_points, teaching_content_de, teaching_content_en,
                           learning_objectives_de, learning_objectives_en, references_de,
                           references_en, option_de, option_en, usability_de, usability_en,
                           prerequisite_participation_de, prerequisite_participation_en,
                           prerequisite_credit_points_de, prerequisite_credit_points_en,
                           grade_improvement_de, grade_improvement_en, diploma_supplement_de,
                           diploma_supplement_en, supplement_form_exam_de, supplement_form_exam_en,
                           comment_de, comment_en, teaching_language_id,
                           duration_id, rotation_id, grading_system_id, weighting_method_id)
VALUES (N'18-tt-1000', N'Abbr1De', N'Abbr1En', N'CertAbbr1De', N'CertAbbr1En', N'Name1De',
        N'Name1En',
        N'Description1De', N'Description1En', 1, N'TeachingContent1De', N'TeachingContent1En',
        N'LearningObjectives1De', N'LearningObjectives1En', N'References1De', N'References1En',
        N'Option1De', N'Option1En', N'Usability1De', N'Usability1En',
        N'PrerequisiteParticipation1De',
        N'PrerequisiteParticipation1En', N'PrerequisiteCreditPoints1De',
        N'PrerequisiteCreditPoints1En', N'GradeImprovement1De', N'GradeImprovement1En',
        N'DiplomaSupplement1De', N'DiplomaSupplement1En', N'SupplementeFormExam1De',
        N'SupplementeFormExam1En', N'Comment1De', N'Comment1En', 1, 1, 1, 1,
        1);
INSERT INTO module_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, description_de,
                           description_en, credit_points, teaching_content_de, teaching_content_en,
                           learning_objectives_de, learning_objectives_en, references_de,
                           references_en, option_de, option_en, usability_de, usability_en,
                           prerequisite_participation_de, prerequisite_participation_en,
                           prerequisite_credit_points_de, prerequisite_credit_points_en,
                           grade_improvement_de, grade_improvement_en, diploma_supplement_de,
                           diploma_supplement_en, supplement_form_exam_de, supplement_form_exam_en,
                           comment_de, comment_en, teaching_language_id,
                           duration_id, rotation_id, grading_system_id, weighting_method_id)
VALUES (N'18-tt-1000', N'Abbr2De', N'Abbr2En', N'CertAbbr2De', N'CertAbbr2En', N'Name2De',
        N'Name2En',
        N'Description2De', N'Description2En', 2, N'TeachingContent2De', N'TeachingContent2En',
        N'LearningObjectives2De', N'LearningObjectives2En', N'References2De', N'References2En',
        N'Option2De', N'Option2En', N'Usability2De', N'Usability2En',
        N'PrerequisiteParticipation2De',
        N'PrerequisiteParticipation2En', N'PrerequisiteCreditPoints2De',
        N'PrerequisiteCreditPoints2En', N'GradeImprovement2De', N'GradeImprovement2En',
        N'DiplomaSupplement2De', N'DiplomaSupplement2En', N'SupplementeFormExam2De',
        N'SupplementeFormExam2En', N'Comment2De', N'Comment2En', 2, 2, 2, 2,
        2);
INSERT INTO module_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, description_de,
                           description_en, credit_points, teaching_content_de, teaching_content_en,
                           learning_objectives_de, learning_objectives_en, references_de,
                           references_en, option_de, option_en, usability_de, usability_en,
                           prerequisite_participation_de, prerequisite_participation_en,
                           prerequisite_credit_points_de, prerequisite_credit_points_en,
                           grade_improvement_de, grade_improvement_en, diploma_supplement_de,
                           diploma_supplement_en, supplement_form_exam_de, supplement_form_exam_en,
                           comment_de, comment_en, teaching_language_id,
                           duration_id, rotation_id, grading_system_id, weighting_method_id)
VALUES (N'38-tt-3000', N'Abbr3De', N'Abbr3En', N'CertAbbr3De', N'CertAbbr3En', N'Name3De',
        N'Name3En',
        N'Description3De', N'Description3En', 3, N'TeachingContent3De', N'TeachingContent3En',
        N'LearningObjectives3De', N'LearningObjectives3En', N'References3De', N'References3En',
        N'Option3De', N'Option3En', N'Usability3De', N'Usability3En',
        N'PrerequisiteParticipation3De',
        N'PrerequisiteParticipation3En', N'PrerequisiteCreditPoints3De',
        N'PrerequisiteCreditPoints3En', N'GradeImprovement3De', N'GradeImprovement3En',
        N'DiplomaSupplement3De', N'DiplomaSupplement3En', N'SupplementeFormExam3De',
        N'SupplementeFormExam3En', N'Comment3De', N'Comment3En', 1, 1, 1, 1,
        1);


-- Module_Version
INSERT INTO module_version (number, activated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_module_version_id)
VALUES (1, N'2020-01-17 11:00:00.0000000 +01:00', N'ModuleVersionName1De',
        N'ModuleVersionName1EN', N'ModuleVersionDescription1De', N'ModuleVersionDescription1En', 1,
        111111111111111);
INSERT INTO module_version (number, activated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_module_version_id)
VALUES (2, N'2020-02-01 12:00:00.0000000 +01:00', N'ModuleVersionName2De',
        N'ModuleVersionName2EN', N'ModuleVersionDescription2De', N'ModuleVersionDescription2En', 2,
        222222222222222);
INSERT INTO module_version (number, activated_at, deactivated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_module_version_id)
VALUES (1, N'2020-03-1 13:00:00.0000000 +01:00', N'2020-03-19 18:00:00.0000000 +01:00',
        N'ModuleVersionName3De', N'ModuleVersionName3EN', N'ModuleVersionDescription3De',
        N'ModuleVersionDescription3En', 3, 333333333333333);


-- Module_Base_has_Module_Version
INSERT INTO module_base_has_module_version (base_id, version_id)
VALUES (1, 1);
INSERT INTO module_base_has_module_version (base_id, version_id)
VALUES (1, 2);
INSERT INTO module_base_has_module_version (base_id, version_id)
VALUES (2, 3);

-- Module_Version_has_responsible_person
INSERT INTO module_version_has_responsible_person (module_version_id, person_id, responsibility_id)
VALUES (1, 1, 1);
INSERT INTO module_version_has_responsible_person (module_version_id, person_id, responsibility_id)
VALUES (1, 2, 1);
INSERT INTO module_version_has_responsible_person (module_version_id, person_id, responsibility_id)
VALUES (1, 3, 2);
INSERT INTO module_version_has_responsible_person (module_version_id, person_id, responsibility_id)
VALUES (2, 1, 1);


-- Course_Base
INSERT INTO course_base (tucan_course_id, activated_at, organization_id)
VALUES (234567890123456, N'2020-01-07 14:11:00.0000000 +01:00', 1);
INSERT INTO course_base (tucan_course_id, activated_at, deactivated_at, organization_id)
VALUES (345678901234567, N'2020-02-07 15:22:00.0000000 +01:00',
        N'2020-03-09 19:22:00.0000000 +01:00', 1);


-- Course_Detail
INSERT INTO course_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, contact_hours_per_week,
                           category_id, type_id, teaching_language_id)
VALUES (N'18-tt-1000-vl', N'Abbr1De', N'Abbr1En', N'CertAbbr1De', N'CertAbbr1En', N'Name1De',
        N'Name1En', 1, 1, 1, 1);
INSERT INTO course_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, contact_hours_per_week,
                           category_id, type_id, teaching_language_id)
VALUES (N'18-tt-1000-vl', N'Abbr2De', N'Abbr2En', N'CertAbbr2De', N'CertAbbr2En', N'Name2De',
        N'Name2En', 2, 2, 2, 2);
INSERT INTO course_detail (number, abbreviation_de, abbreviation_en, certificate_abbreviation_de,
                           certificate_abbreviation_en, name_de, name_en, contact_hours_per_week,
                           category_id, type_id, teaching_language_id)
VALUES (N'38-tt-3000-vl', N'Abbr3De', N'Abbr3En', N'CertAbbr3De', N'CertAbbr3En', N'Name3De',
        N'Name3En', 3, 1, 1, 1);


-- Course_Version
INSERT INTO course_version (number, activated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_course_version_id)
VALUES (1, N'2020-01-07 11:00:00.0000000 +01:00', N'CourseVersionName1De',
        N'CourseVersionName1EN', N'CourseVersionDescription1De', N'CourseVersionDescription1En', 1,
        111111111111111);
INSERT INTO course_version (number, activated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_course_version_id)
VALUES (2, N'2020-02-01 12:00:00.0000000 +01:00', N'CourseVersionName2De',
        N'CourseVersionName2EN', N'CourseVersionDescription2De', N'CourseVersionDescription2En', 2,
        222222222222222);
INSERT INTO course_version (number, activated_at, deactivated_at, name_de, name_en, description_de,
                            description_en, detail_id, tucan_course_version_id)
VALUES (1, N'2020-02-07 13:00:00.0000000 +01:00', N'2020-03-08 18:00:00.0000000 +01:00',
        N'CourseVersionName3De', N'CourseVersionName3EN', N'CourseVersionDescription3De',
        N'CourseVersionDescription3En', 3, 333333333333333);


-- Course_Base_has_Course_Version
INSERT INTO course_base_has_course_version (base_id, version_id)
VALUES (1, 1);
INSERT INTO course_base_has_course_version (base_id, version_id)
VALUES (1, 2);
INSERT INTO course_base_has_course_version (base_id, version_id)
VALUES (2, 3);


-- Course_Version_has_responsible_Person
INSERT INTO course_Version_has_responsible_Person (course_version_id, person_id, responsibility_id)
VALUES (1, 1, 1);
INSERT INTO course_Version_has_responsible_Person (course_version_id, person_id, responsibility_id)
VALUES (1, 2, 1);
INSERT INTO course_Version_has_responsible_Person (course_version_id, person_id, responsibility_id)
VALUES (1, 3, 2);
INSERT INTO course_Version_has_responsible_Person (course_version_id, person_id, responsibility_id)
VALUES (2, 1, 1);


-- Module_Version_has_Course_Version
INSERT INTO module_version_has_course_version (module_version_id, course_version_id)
VALUES (1, 1);
INSERT INTO module_version_has_course_version (module_version_id, course_version_id)
VALUES (1, 2);
INSERT INTO module_version_has_course_version (module_version_id, course_version_id)
VALUES (2, 3);


-- Exam_Base
INSERT INTO exam_base (tucan_module_id, activated_at, organization_id)
VALUES (123456789012345, N'2020-01-07 14:40:02.0000000 +01:00', 1);
INSERT INTO exam_base (tucan_module_id, tucan_course_id, activated_at, deactivated_at,
                       organization_id)
VALUES (123456789012345, 234567890123456, N'2020-02-07 13:42:03.0000000 +01:00',
        N'2020-03-09 19:00:19.0000000 +01:00', 1);


-- Exam_Detail
INSERT INTO exam_detail (number, name_de, name_en, mandatory_to_pass, duration,
                         mark_included_in_grading, weighting_of_mark, explicit_registration,
                         category_id, type_id, grading_system_id, term_id)
VALUES (N'18-tt-1000-vl', N'ExamDetail1De', N'ExamDetail1En', 1, 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO exam_detail (number, name_de, name_en, mandatory_to_pass, duration,
                         mark_included_in_grading, weighting_of_mark, explicit_registration,
                         category_id, type_id, grading_system_id, term_id)
VALUES (N'18-tt-1000-vl', N'ExamDetail2De', N'ExamDetail2En', 0, 2, 0, 2, 0, 2, 2, 2, 2);
INSERT INTO exam_detail (number, name_de, name_en, mandatory_to_pass, duration,
                         mark_included_in_grading, weighting_of_mark, explicit_registration,
                         category_id, type_id, grading_system_id, term_id)
VALUES (N'38-tt-3000-vl', N'ExamDetail3De', N'ExamDetail3En', 1, 3, 0, 3, 1, 1, 1, 1, 1);


-- Exam_Version
INSERT INTO exam_version (number, activated_at, name_de, name_en, description_de,
                          description_en, detail_id)
VALUES (1, N'2020-01-07 11:00:00.0000000 +01:00', N'ExamVersionName1De',
        N'ExamVersionName1EN', N'ExamVersionDescription1De', N'ExamVersionDescription1En', 1);
INSERT INTO exam_version (number, activated_at, name_de, name_en, description_de,
                          description_en, detail_id)
VALUES (2, N'2020-02-01 12:00:00.0000000 +01:00', N'ExamVersionName2De',
        N'ExamVersionName2EN', N'ExamVersionDescription2De', N'ExamVersionDescription2En', 2);
INSERT INTO exam_version (number, activated_at, deactivated_at, name_de, name_en, description_de,
                          description_en, detail_id)
VALUES (1, N'2020-02-07 13:00:00.0000000 +01:00', N'2020-03-08 18:00:00.0000000 +01:00',
        N'ExamVersionName3De', N'ExamVersionName3EN', N'ExamVersionDescription3De',
        N'ExamVersionDescription3En', 3);


-- Exam_Base_has_Exam_Version
INSERT INTO exam_base_has_exam_version (base_id, version_id)
VALUES (1, 1);
INSERT INTO exam_base_has_exam_version (base_id, version_id)
VALUES (1, 2);
INSERT INTO exam_base_has_exam_version (base_id, version_id)
VALUES (2, 3);

-- Exam_version_has_tucan_exam_id
INSERT INTO exam_version_has_tucan_exam_id (exam_version_id, tucan_exam_id)
VALUES (1, 111111111111111);
INSERT INTO exam_version_has_tucan_exam_id (exam_version_id, tucan_exam_id)
VALUES (1, 123111111111111);
INSERT INTO exam_version_has_tucan_exam_id (exam_version_id, tucan_exam_id)
VALUES (2, 222222222222222);


-- Exam_Version_has_responsible_Person
INSERT INTO exam_Version_has_responsible_Person (exam_version_id, person_id, responsibility_id)
VALUES (1, 1, 1);
INSERT INTO exam_Version_has_responsible_Person (exam_version_id, person_id, responsibility_id)
VALUES (1, 2, 1);
INSERT INTO exam_Version_has_responsible_Person (exam_version_id, person_id, responsibility_id)
VALUES (1, 3, 2);
INSERT INTO exam_Version_has_responsible_Person (exam_version_id, person_id, responsibility_id)
VALUES (2, 1, 1);


-- Module_Version_has_Exam_Version
INSERT INTO module_version_has_exam_version (module_version_id, exam_version_id)
VALUES (1, 1);
INSERT INTO module_version_has_exam_version (module_version_id, exam_version_id)
VALUES (1, 2);
INSERT INTO module_version_has_exam_version (module_version_id, exam_version_id)
VALUES (2, 3);


-- Data_Import_Log
INSERT INTO data_import_log (successful, started_at, finished_at, final_result_de,
                             final_result_en, info_message, warn_message, error_message,
                             debug_message, from_external_system_id, imported_external_data_id)
VALUES (1, N'2021-01-01 18:11:00.0000000 +01:00', N'2021-01-02 02:11:00.0000000 +01:00',
        N'Final Result 1 De', N'Final Result 1 En', N'Info Message 1', N'Warn Message 1',
        N'Error Message 1', N'Debug Message 1', 1, 1);
INSERT INTO data_import_log (successful, started_at, finished_at, final_result_de,
                             final_result_en, info_message, warn_message, error_message,
                             debug_message, from_external_system_id, imported_external_data_id)
VALUES (0, N'2021-02-01 18:11:00.0000000 +01:00', N'2021-02-02 02:11:00.0000000 +01:00',
        N'Final Result 2 De', N'Final Result 2 En', N'Info Message 2', N'Warn Message 2',
        N'Error Message 2', N'Debug Message 2', 1, 2);
INSERT INTO data_import_log (successful, started_at, finished_at, final_result_de,
                             final_result_en, info_message, warn_message, error_message,
                             debug_message, from_external_system_id, imported_external_data_id)
VALUES (1, N'2021-03-01 18:11:00.0000000 +01:00', N'2021-03-02 02:11:00.0000000 +01:00',
        N'Final Result 3 De', N'Final Result 3 En', N'Info Message 3', N'Warn Message 3',
        N'Error Message 3', N'Debug Message 3', 2, 3);